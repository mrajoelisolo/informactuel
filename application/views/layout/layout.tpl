<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{var_page_title}</title>
    <link rel="shortcut icon" type="image/jpg" href="{path_img}favicon.jpg" />
    {css_resources}
    <link rel="stylesheet" type="text/css" href="{var_resource}" media="all" />{/css_resources}

    <!--[if lt IE 7]>
     <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE7.js"  type="text/javascript"></script>
     <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE7-squish.js" type="text/javascript"></script>
    <![endif]-->
    {js_resources}
    <script type="text/javascript" src="{var_resource}"></script>{/js_resources}

    {js_literals}{var_literal_js}{/js_literals}
</head>
    <div class="general_content">
        <div class="header">
            {var_header_zone} 
        </div>
        
        <div class="sliderbox">
            {var_slider_zone} 
        </div>
        
        <div class="leftcolumn">
            {var_left_zone}  
        </div>
        
        <div class="centercolumn">
            {var_center_zone}  
        </div>
        
        <div class="rightcolumn">
            {var_right_zone}  
        </div>
        
        <div class="spacer"></div>
        
        <div class="footer">
            {var_footer_zone}
        </div>
    </div>
</body>
</html>