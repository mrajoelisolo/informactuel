       		<div class="resultheadbox">
       			<p class="txtresult">Résultat de la recherche pour</p>
                <p class="keywords">"{keywords}"</p>
            </div>
            
            {var_movies}
            <p><a href="{base_url}movies/movie_details/fetch/{movie_ref}"  class="searchmatch">{movie_title}</a></p>
            <p class="matchcategory">{category_name}</p>
            {/var_movies}
           
            <div class="spacer"></div>
            
            
            <!---------------------------------- PAGINATION ---------------------------------->
            <div class="wrap-pagination">
                {var_pagination}
            </div>
            <!---------------------------------- FIN PAGINATION ---------------------------------->
          
          <div class="spacer"></div>  