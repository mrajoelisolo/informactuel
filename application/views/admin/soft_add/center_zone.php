<div id="sidebar_container">
    <div class="sidebar">
        <h3></h3>
        <div class="sidebar_item">
            <p>
                <input class="button purple" type="button" value="Fermer la session" onclick="logout();">
            </p>
        </div>
    </div>
</div>
<div class="content">
    <h1>Ajout d'un nouveau jeu</h1>
    <form id="form-soft" action="{base_url}admin/soft_add/save" enctype="multipart/form-data" method="post">
        <input name="MAX_FILE_SIZE" type="hidden" value="1000000" />
        <table class="tbl-data">
            <thead>
                <tr>
                    <th colspan="2">General</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="col1"><strong>Soft title</strong></td>
                    <td class="col2"><input type="text" id="soft_title" name="soft_title" class="input-txt"></td>
                </tr>
                
                <tr>
                    <td><strong>Soft reference</strong></td>
                    <td><input type="text" id="soft_ref" name="soft_ref"></td>
                </tr>
                
                <tr>
                    <td><strong>Soft category</strong></td>
                    <td>
                        <select id="soft_category" class="select-cmb" name="soft_category">
                            {var_soft_categories}
                            <option value="{category_id}">{category_name}</option>
                            {/var_soft_categories}
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td><strong>Soft description</strong></td>
                    <td><textarea id="soft_description" name="soft_description"></textarea></td>
                </tr>
                
                <tr>
                    <td><strong>Platform</strong></td>
                    <td>
                        <select id="soft_platform" class="select-cmb" name="soft_platform">
                            {var_soft_platforms}
                            <option value="{platform_name}">{platform_name}</option>
                            {/var_soft_platforms}
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td><strong>Soft cover</strong></td>
                    <td><input type="file" id="soft_file_image" name="soft_file_image" accept="image/*"></td>
                </tr>
                
                <tr>
                    <td><strong>Soft link</strong></td>
                    <td><input class="input-txt" type="text" id="soft_link" name="soft_link"></td>
                </tr>
                
                <tr>
                    <td><strong>Minimal config</strong></td>
                    <td><textarea id="soft_min_config" name="soft_min_config"></textarea></td>
                </tr>
                
                <tr>
                    <td><strong>Supports</strong></td>
                    <td>
                        <input type="checkbox" id="soft_dvd" name="soft_dvd" value="soft_dvd"> DVD<br>
                        <input type="checkbox" id="soft_cd" name="soft_cd" value="soft_cd"> CD
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Enregistrer" class="button green">
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="div-message">
            <img id="ajax-loading" src="{path_img}ajax-loader.gif" style="display:none;">
            <span id="ajax-message"></span>
        </div>
    </form>
</div>