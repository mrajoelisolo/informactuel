<div id="logo">
    <div id="logo_text">
        <h1>Inform'Actuel</h1>
        <h2>La r&eacute;f&eacute;rence en jeux, logiciels, films et s&eacute;ries TV</h2>
    </div>
</div>
<nav>
    <ul class="sf-menu" id="nav">
        <li><a href="{base_url}admin/homepage">Accueil</a></li>
        <li><a href="#">Nouveau logiciel</a></li>
        <li><a href="{base_url}admin/soft_list/fetch/pc">Liste des logiciels PC</a></li>
        <li><a href="{base_url}admin/soft_list/fetch/mac">Liste des logiciels MAC</a></li>
    </ul>
</nav>
{modal_confirm_logout}