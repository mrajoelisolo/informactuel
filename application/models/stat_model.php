<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat_Model extends CI_Model
{
    const TABLE_GAME = 'game';
    const TABLE_MOVIE = 'movie';
    const TABLE_SERIE = 'movie';
    const TABLE_SOFT = 'soft';
    
    /** Games **/
    public function getIncorrectGameReferences($platform = 'PC')
    {
        
        $res = $this->db->select('*')
                    ->from(self::TABLE_GAME)
                    ->where('game_platform', $platform)
                    ->not_like('game_ref', 'PC', 'after')
                    ->get()
                    ->result();
        
        return $res;
    }
    
    public function countIncorrectGameReferences($platform = 'PC')
    {
        $row = $this->db->select('COUNT(*) AS count')
                    ->from(self::TABLE_GAME)
                    ->where('game_platform', $platform)
                    ->not_like('game_ref', 'PC', 'after')
                    ->get()
                    ->row();
        
        return $row->count;
    }
    
    /** Movies **/
    public function getIncorrectMovieReferences()
    {
        $res = $this->db->select('*')
                    ->from(self::TABLE_MOVIE)
                    ->where('movie_type', 'Film')
                    ->not_like('movie_ref', 'F', 'after')
                    ->get()
                    ->result();
        
        return $res;
    }
    
    public function countIncorrectMovieReferences()
    {
        $row = $this->db->select('COUNT(*) AS count')
                    ->from(self::TABLE_MOVIE)
                    ->where('movie_type', 'Film')
                    ->not_like('movie_ref', 'F', 'after')
                    ->get()
                    ->row();
        
        return $row->count;
    }
    
    /** Series **/
    public function getIncorrectSerieReferences()
    {
        $res = $this->db->select('*')
                    ->from(self::TABLE_SERIE)
                    ->where('movie_type', 'Série')
                    ->not_like('movie_ref', 'TV', 'after')
                    ->get()
                    ->result();
        
        return $res;
    }
    
    public function countIncorrectSerieReferences()
    {
        $row = $this->db->select('COUNT(*) AS count')
                    ->from(self::TABLE_SERIE)
                    ->where('movie_type', 'S&eacute;rie')
                    ->not_like('movie_ref', 'TV', 'after')
                    ->get()
                    ->row();
        
        return $row->count;
    }
    
    /** Softs **/
    public function getIncorrectSoftReferences($platform = 'PC')
    {
        $res = $this->db->select('*')
                    ->from(self::TABLE_SOFT)
                    ->where('soft_platform', $platform)
                    ->not_like('movie_ref', 'L', 'after')
                    ->get()
                    ->result();
        
        return $res;
    }
    
    public function countIncorrectSoftReferences($platform = 'PC')
    {
        $row = $this->db->select('COUNT(*) AS count')
                    ->from(self::TABLE_SOFT)
                    ->where('soft_platform', $platform)
                    ->not_like('movie_ref', 'L', 'after')
                    ->get()
                    ->row();
        
        return $row->count;
    }
}