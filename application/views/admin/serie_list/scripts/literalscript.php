<script type="text/javascript">
    $('#form_search').submit(function(e) {
        e.preventDefault();
        
        var currentPlatform = $('#txt_current_platform').val();
        
        var f_movie_ref = $('#txt_movie_ref').val();
        var f_movie_title = $('#txt_movie_title').val();
        
        var criteria = '';
        if( f_movie_ref != '' )
            criteria += 'ref/' +  f_movie_ref;
            
        if( f_movie_title != '' ) {
            if(criteria != '')
                criteria += '/';
            
            criteria += 'title/' +  f_movie_title;
        }
        
        document.location = '{base_url}/admin/serie_list/fetch/' + currentPlatform + '/' + criteria;
    });
    
    function delete_movie(movie_ref)
    {
        var idDialog = '#dialog_confirm_remove_serie'; 
        $( idDialog ).dialog({
            resizable: false,
            height:140,
            modal: true,
 			buttons: {
                "Confirmer": function() {
                    $.ajax({
                        url: "{base_url}admin/serie_edit/delete",
                        type: "post",
                        dataType: "json",
                        data: { ref: movie_ref },
                        success: function(res) {
                            $( idDialog ).dialog( "close" );
    
                            if( res.error == 0 )
                            {
                                var row = $('#' + movie_ref);
                                $(row).remove();
                            }
                        }
                    });
                },
                "Annuler": function() {
                    $( idDialog ).dialog( "close" );
                }
            }
        });
    }
    
    function edit_movie(movie_ref)
    {
        document.location = '{base_url}admin/serie_edit/edit/' + movie_ref;
    }
    
    $(function() {

    });
</script>