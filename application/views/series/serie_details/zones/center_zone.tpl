{var_movie}
       		<div class="detailsbox">
            	<div class="detailsboxleft">
                	<div class="headtitlebox"><!-- début titre -->
                    	<p class="headtitle"><a class="filmtitle"href="#">{movie_title}</a></p>
                    </div><!-- fin titre -->
                    <p class="cover"><img src="{path_img}movies/thumbnails/{var_movie_cover}" /></p>
                	<p class="infos">{movie_description}</p>
<div class="spacer"></div>
					<p class="labacteurs">Acteurs principaux</p>
                    <p class="acteurs">{movie_actors_tag}</p>
                </div>
                
                <div class="detailboxright">
                        <div class="infobox">
                            <p>Catégorie :</p>
                            <p class="valuecategorie">{category_name}</p>
                            
            <p>Référence :</p>
                            <p class="valueref">{movie_ref}</p>
                            
            <p>Réalisateur :</p>
                            <p class="valuerealisateur">{movie_director}</p>
                            
            <p>Sortie :</p>
                            <p class="valuesortie">{movie_release_year}</p>
                            
                            <p>Durée :</p>
                            <p class="valueduration">{movie_duration_minute} minutes</p>
                            
                      </div>
                      {if_support_dvd}
                      <div class="support">
                      	<img class="supcd" src="{support_type}" alt="Type support" title="Type support" />
                      </div>
                      {/if_support_dvd}
                      {if_support_cd}
                      <div class="support">
                      	<img class="supcd" src="{support_type}" alt="Type support" title="Type support" />
                      </div>
                      {/if_support_cd}
                </div>
                
                <div class="spacer"></div>
                
            </div>
            
            <div class="apercufilm">{movie_preview}</div>
{/var_movie}
           
            <div class="spacer"></div>
          
          <div class="spacer"></div>  
            
            <!-- //////////////////////////////////////// FIN COLONNE CENTRALE //////////////////////////////////////// -->