<div id="sidebar_container">
    <div class="sidebar">
        <h3>Actions</h3>
        <div class="sidebar_item">
            <p>
                <input class="button purple" type="button" value="Fermer la session" onclick="logout();">
            </p>
        </div>
    </div>
</div>
<div class="content">
    <h1>Liste des jeux</h1>
    
    <div class="content_item">
        <form id="form_search" method="post" action="#">
            <input type="hidden" id="txt_current_platform" value="{var_current_platform}">
            <p>
                <label for="game_ref"><strong>R&eacute;f&eacute;rence du jeu: </strong></label>
                <input type="text" name="txt_game_ref" id="txt_game_ref">
                <label for="game_ref"><strong>Titre du jeu: </strong></label>
                <input type="text" name="txt_game_title" id="txt_game_title">
            </p>
            <p>
                <input class="button darkblue" type="submit" value="Rechercher">
            </p>
        </form>
    </div>
    
    <div class="wrap-pagination">
        {var_pagination}
    </div>
    
    <table class="tbl-data">
        <thead>
            <tr>
                <th>R&eacute;f&eacute;rence</th>
                <th>Titre</th>
                <th>Cat&eacute;gorie</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            {var_games}
            <tr id="{game_ref}">
                <td>{game_ref}</td>
                <td>{game_title}</td>
                <td>{category_name}</td>
                <td>
                    <input class="button green" type="button" value="Modifier" onclick="edit_game('{game_ref}');">
                    <input class="button orange" type="button" value="Supprimer" onclick="delete_game('{game_ref}');">
                </td>
            </tr>
            {/var_games}
        </tbody>
    </table>
    
    <div class="wrap-pagination">
        {var_pagination}
    </div>
</div>