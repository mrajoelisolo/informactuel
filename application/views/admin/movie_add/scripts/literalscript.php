<script type="text/javascript">
    function validateForm()
    {
        var isValid = true;
        
        var fields = ['movie_title', 'movie_ref'];
        for( i = 0; i < fields.length; i++ )
        {
            var field = fields[i];
            var oField = $('#' + field);
            if( oField.val().trim() == '' )
            {
                oField.addClass('invalid-field');
                isValid = false;
            }
            else
            {
                oField.removeClass('invalid-field');
            }
        }
        
        //Check the numeric fields
        var nfields = ['movie_release_year', 'movie_duration_minute'];
        for( i = 0; i < nfields.length; i++ )
        {
            var field = nfields[i];
            var oField = $('#' + field);
            var nFieldVal = oField.val();
            
            if( ! isNumber(nFieldVal) )
            {
                oField.addClass('invalid-field');
                isValid = false;
            }
            else
            {
                oField.removeClass('invalid-field');
            }
        } 
        
        if( ! isValid )
        {
            $('#ajax-message').addClass('error-message');
            $('#ajax-message').html('Veuillez verifier les champs invalides.');
        }
        else
        {
            $('#ajax-message').removeClass('error-message');
            $('#ajax-message').empty();
        }
        
        return isValid;
    }

    $(function() {
        $('#form-movie').submit(function(e) {
            var res = validateForm();
         
            $('#ajax-message').removeClass('confirmation-message');   
            if( ! res ) {
                e.preventDefault();
            }
        });
        
        $('#form-movie').ajaxForm({
            dataType: 'json',
            beforeSubmit: function() {
                $('#ajax-loading').show();
            },
            success: function(data) {
                $('#ajax-loading').hide();
                $('#ajax-message').show();
                
                if( data.error == 0 )
                {
                    $('#ajax-message').html(data.message + ' cliquer <a href="{base_url}admin/movie_edit/by_ref/' + data.ref + '" target="_blank">ici</a> pour voir');
                    $('#ajax-message').wrapInner('<strong></strong>').addClass('confirmation-message');
                    
                    //Clear the fields
                    $('#movie_title').val('');
                    $('#movie_ref').val('');
                    $('#movie_description').val('');
                    $('#movie_file_image').val('');
                    $('#movie_preview').val('');
                    $('#movie_actors_tag').val('');
                    $('#movie_dvd').attr('checked', false);
                    $('#move_cd').attr('checked', false);
                    $('#movie_director').val('');
                    $('#movie_release_year').val('');
                    $('#movie_duration_minute').val('');
                }
                else
                {
                    $('#ajax-message').html(data.message);
                    $('#ajax-message').wrapInner('<strong></strong>').addClass('error-message');
                }
            }
        });
    });
</script>