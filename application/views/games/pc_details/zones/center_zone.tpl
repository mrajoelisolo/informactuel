{var_game}
       		<div class="detailsbox">
            	<div class="detailsboxleft">
                	<div class="headtitlebox"><!-- début titre -->
                    	<p class="headtitle"><a class="pcgame"href="#">{game_title}</a></p>
                    </div><!-- fin titre -->
                    <p class="cover"><img src="{var_game_thumbnail}" /></p>
                	<p class="infos">{game_config_min}</p>
                    <div class="spacer"></div>
                </div>
                
                
                
                <div class="detailboxright">
                        <div class="infobox">
                            <p>Genre :</p>
                            <p class="valuegenre">{category_name}</p>
                            
            <p>Référence :</p>
                            <p class="valueref">{game_ref}</p>
                            
            <p>Editeur :</p>
                            <p class="valueediteur">nom de l'éditeur</p>
                            
                      </div>
                      {if_support_dvd}
                      <div class="support">
                      	<img class="supcd" src="{support_type}" alt="Type support" title="Type support" />
                      </div>
                      {/if_support_dvd}
                      {if_support_cd}
                      <div class="support">
                      	<img class="supcd" src="{support_type}" alt="Type support" title="Type support" />
                      </div>
                      {/if_support_cd}
                </div>
                
                <div class="spacer"></div>
                
            </div>
            <div>
            	<a href="{game_link}" class="linkdetails">plus de détails</a>
                <div class="spacer"></div>
            </div>
{/var_game}