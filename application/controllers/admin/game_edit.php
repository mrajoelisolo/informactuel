<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_Edit extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('game_model');
        $this->load->model('game_category_model');
        $this->load->model('game_platform_model');
    }
    
    public function edit($game_ref) {
        if($this->session->userdata('user_login'))
        {
            $pageTitle = 'Inform\'Actuel Admin - Edition de jeux';
            $folderPath = 'admin/game_edit/';
    
            $cssResources = array(
                array('var_resource' => path_css() . 'style.css'),
                array('var_resource' => path_css() . 'modal.css'),
                array('var_resource' => path_css() . 'tbldata.css'),
                array('var_resource' => path_css() . 'formvalidation.css'),
                array('var_resource' => path_css() . 'simplebutton.css'),
                
                array('var_resource' => path_css() . 'ui/jquery.ui.all.css'),
            );
            
            $jsResources = array(
                array('var_resource' => path_js() . 'jquery.min.js'),
                array('var_resource' => path_js() . 'jquery.easing-sooper.js'),
                array('var_resource' => path_js() . 'jquery.sooperfish.js'),
                array('var_resource' => path_js() . 'main.js'),
                array('var_resource' => path_js() . 'jquery.form.js'),
                
                array('var_resource' => path_js() . 'ui/jquery-ui.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.core.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.widget.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.button.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.mouse.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.draggable.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.position.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.dialog.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.datepicker.js'),
            );
            
            $jsLiterals = array(
                array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.php', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse('admin/portal/scripts/logout.php', assets_paths(), TRUE)),
            );
            
            /* Setting the header content */
            
            $headerData = assets_paths();
            $headerData = array_merge($headerData, array(
                'modal_confirm_logout' => $this->parser->parse('admin/portal/modal/confirm_logout', assets_paths(), TRUE),
            ));
            
            /* Setting the center content */
            $var_game_categories = $this->game_category_model->fetch_array_categories();
            $var_game_platforms = $this->game_platform_model->get_array_platforms();
            $arr_selected_game = $this->game_model->fetch_array_one_game_by_ref($game_ref);
                    
            for( $i = 0; $i < count($var_game_categories); $i++ )
            {
                if( $var_game_categories[$i]['category_name'] == $arr_selected_game['category_name'] )
                {
                    $var_game_categories[$i]['selected_game_category'] = 'selected="selected"';
                }
                else
                {
                    $var_game_categories[$i]['selected_game_category'] = '';
                }
            }
            
            for( $i = 0; $i < count($var_game_platforms); $i++ )
            {
                if( $var_game_platforms[$i]['platform_name'] == $arr_selected_game['game_platform'] )
                {
                    $var_game_platforms[$i]['selected_game_platform'] = 'selected="selected"';
                }
                else
                {
                    $var_game_platforms[$i]['selected_game_platform'] = '';
                }
            }
                    
            $centerData = assets_paths();
            $centerData = array_merge($centerData, array(
                'var_game_categories' => $var_game_categories,
                'var_game_platforms' => $var_game_platforms,
                'var_game_title' => $arr_selected_game['game_title'],
                'var_game_ref' => $arr_selected_game['game_ref'],
                'var_game_description' => $arr_selected_game['game_description'],
                'var_game_link' => $arr_selected_game['game_link'],
                'var_config_min' => $arr_selected_game['game_config_min'],
                'var_dvd_checked' => ($arr_selected_game['game_dvd'] == 1) ? 'checked="checked"' : "" ,
                'var_cd_checked' => ($arr_selected_game['game_cd'] == 1) ? 'checked="checked"' : "" ,
            ));
            
            /* Setting the footer content */
            $footerData = assets_paths();
            $footerData = array_merge($footerData, array(
            ));
            
            $var_header_zone = $this->parser->parse($folderPath . 'header_zone', $headerData, TRUE);
            $var_center_zone = $this->parser->parse($folderPath . 'center_zone', $centerData, TRUE);
            $var_footer_zone = $this->parser->parse('footer_zone', $footerData, TRUE);
            
            $layoutData = assets_paths();
            $layoutData = array_merge($layoutData, array(
                'var_page_title' => $pageTitle,
                'css_resources' => $cssResources,
                'js_resources' => $jsResources,
                'js_literals' => $jsLiterals,
                'var_header_zone' => $var_header_zone,
                'var_center_zone' => $var_center_zone,
                'var_footer_zone' => $var_footer_zone,
            ));
            
            $this->parser->parse('layout', $layoutData, FALSE);
        }
        else
        {
            redirect('/admin/portal');
        }
    }

/*
    public function update()
    {
        $response = array(
            'error'   => 0,
            'message' => 'Geronimo',
        );
        
        echo json_encode($response);
    }
*/

    public function update()
    {
        $game_old_ref     = $this->input->post('game_old_ref');
        $game_title       = $this->input->post('game_title');
        $game_ref         = $this->input->post('game_ref');
        $category_fk    = $this->input->post('game_category');
        $game_description = $this->input->post('game_description');
        $game_platform    = $this->input->post('game_platform');
        $game_link        = $this->input->post('game_link');
        $game_config_min  = $this->input->post('game_min_config');
        $game_dvd         = ($this->input->post('game_dvd')) ? 1 : 0;
        $game_cd          = ($this->input->post('game_cd')) ? 1 : 0;
        
        $ref_exists = $this->game_model->check_if_ref_exists($game_ref);
        if( $ref_exists )
        {
            $response = array(
                'error'   => 1,
                'message' => 'La r&eacute;f&eacute;rence &eacute;xiste d&eacute;ja',
            );
            
            echo json_encode($response);
            return;
        }
        
        $filename = NULL;
        $response = array(
            'error'   => 0,
            'game_ref' => $game_ref,
            'game_platform' => $game_platform,
            'message' => 'Mise a jour effectu&eacute;e',
        );
        
        if( isset($_FILES['game_file_image']) )
        {
            /* Remove first the ancient file */
            $arr_old_game = $this->game_model->fetch_array_one_game_by_ref($game_old_ref);
            $old_filename = $arr_old_game['game_thumbnail1'];
            
            if( ! empty($old_filename) )
            {
                $old_filepath = './assets/images/games/thumbnails/' . $old_filename; 
                if( file_exists($old_filepath) )
                {
                    unlink($old_filepath);
                }
            }            
            
            $config = array(
                'upload_path'   => './assets/images/games/thumbnails/',
                'allowed_types' => 'png|jpg|gif',
                'file_name'     => 'game_' . strtolower($game_platform) . '_' . sha1($_FILES['game_file_image']['name']), 
            );
            
            $this->upload->initialize($config);
            
            if( ! $this->upload->do_upload('game_file_image') )
            {
                $error = strip_tags($this->upload->display_errors());
                
                $response['error'] = 1;
                $response['message'] = $error;
            }
            else
            {
                $fileInfo = array(
                    'upload_data' => $this->upload->data(),
                );
                
                $filename = $fileInfo['upload_data']['file_name'];
                $img_url = base_url() . 'assets/images/games/thumbnails/' . $filename;

                $response['error'] = 0;
                $response['image'] = $img_url;
                
                //Resize the image
                $imgConfig = array(
                    'image_library'  => 'gd2',
                    'source_image'   => './assets/images/games/thumbnails/' . $filename,
                    'maintain_ratio' => TRUE,
                    'width'          => 256,
                    'height'         => 144,
                );
                
                $this->image_lib->initialize($imgConfig);
                $this->image_lib->resize();
            }
        }
        
        
        
        //Insert the game info to the database
        $this->game_model->update_game_by_ref($game_ref, $game_old_ref, $game_title, $game_description, $game_platform, $filename,
            $game_link, $game_config_min, $game_dvd, $game_cd, $category_fk
        );
        
        echo json_encode($response);
    }
    
    public function delete()
    {
        $game_ref = $this->input->post('ref');
        
        $this->game_model->delete_game_by_ref($game_ref);
        
        $response = array (
            'error' => 0,
        );
        
        echo json_encode($response);
    }
}