<div id="logo">
    <div id="logo_text">
        <h1>Inform'Actuel</h1>
        <h2>La r&eacute;f&eacute;rence en jeux, logiciels, films et s&eacute;ries TV</h2>
    </div>
</div>
<nav>
    <ul class="sf-menu" id="nav">
        <li><a href="{base_url}admin/homepage">Accueil</a></li>
        <li><a href="{base_url}admin/movie_add">Nouveau film/série</a></li>
        <li><a href="{base_url}admin/movie_list">Liste des films</a></li>
        <li><a href="{base_url}admin/serie_list">Liste des s&eacute;ries</a></li>
    </ul>
</nav>
{modal_confirm_logout}

<div id="dialog_confirm_remove_movie" title="Suppression d'une série" style="display: none;">
    <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Etes vous sur de vouloir supprimer le film ?</p>
</div>
