<script type="text/javascript">
    $(function() {
        $('#form_login').submit(function(e) {
            e.preventDefault();
            
            $('#ajax_loading').show();
            
            $.ajax({
                url: "{base_url}admin/portal/authentify",
                type: "post",
                dataType: "json",
                data: $("#form_login").serialize(),
                success: function(res) {
                    console.log(res);
                    
                    $('#ajax_loading').hide();
                    if( res.logged == 1 )
                    {
                        document.location = res.success_url;
                    }
                    else
                    {
                        $('#ajax_message').html(res.msg);
                        $('#ajax_message').wrapInner('<strong></strong>').addClass('error-message');
                    }
                }
            });
        });
    });
</script>