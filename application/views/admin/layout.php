<!DOCTYPE HTML>
<html>
<head>
    <title>{var_page_title}</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    {css_resources}
    <link rel="stylesheet" type="text/css" href="{var_resource}" />{/css_resources}
</head>
<body>
    <div id="main">
        <div id="site_content">
            {var_center_zone}
        </div>
    </div>
    
    {js_resources}
    <script type="text/javascript" src="{var_resource}"></script>{/js_resources}
    {js_literals}{var_literal_js}{/js_literals}
</body>
</html>