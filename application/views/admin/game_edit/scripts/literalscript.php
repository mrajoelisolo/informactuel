<script type="text/javascript">
    function validateForm()
    {
        var isValid = true;
        
        var fields = ['game_title', 'game_ref', 'game_link'];
        for( i = 0; i < fields.length; i++ )
        {
            var field = fields[i];
            var oField = $('#' + field);
            if( oField.val().trim() == '' )
            {
                oField.addClass('invalid-field');
                isValid = false;
            }
            else
            {
                oField.removeClass('invalid-field');
            }
        }
        
        if( ! isValid )
        {
            $('#ajax-message').addClass('error-message');
            $('#ajax-message').html('Veuillez remplir les champs obligatoires.');
        }
        else
        {
            $('#ajax-message').removeClass('error-message');
            $('#ajax-message').empty();
        }
        
        return isValid;
    }

    $(function() {
        $('#form-game').submit(function(e) {
            var res = validateForm();
         
            $('#ajax-message').removeClass('confirmation-message');   
            if( ! res ) {
                e.preventDefault();
            }
        });
        
        $('#form-game').ajaxForm({
            dataType: 'json',
            beforeSubmit: function() {
                $('#ajax-loading').show();
            },
            success: function(data) {
                $('#ajax-loading').hide();
                $('#ajax-message').show();
                
                $('#ajax-message').html(data.message);
                if( data.error == 0 )
                {
                    document.location = '{base_url}admin/game_list/fetch/' + data.game_platform;
                    
                    //Clear the fields
                    $('#game_title').val('');
                    $('#game_ref').val('');
                    $('#game_description').val('');
                    $('#game_file_image').val('');
                    $('#game_link').val('');
                    $('#game_min_config').val('');
                    $('#game_dvd').attr('checked', false);
                    $('#game_cd').attr('checked', false);
                }
                else
                {
                    $('#ajax-message').wrapInner('<strong></strong>').addClass('error-message');
                }
            }
        });
    });
</script>