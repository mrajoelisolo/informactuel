<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie_List extends CI_Controller
{
    const COUNT_PER_PAGE = 5;

    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('pagination');
        
        $this->load->model('movie_model');
        $this->load->model('movie_category_model');
        $this->load->model('movie_type_model');
    }
    
    public function index()
    {
        $this->fetch('film');
    }
    
    public function fetch($movie_type)
    {
        if($this->session->userdata('user_login'))
        {
            $params = $this->uri->uri_to_assoc(3);
            
            $f_ref = ( isset($params['ref'])) ? $params['ref'] : '';
            $f_title = ( isset($params['title'])) ? $params['title'] : '';
            $f_category = ( isset($params['category'])) ? $params['category'] : '';
            $start = ( isset($params['start'])) ? $params['start'] : 0;
            
            $segments = '';
            if( trim($f_ref) != '' )
            {
                $segments .= 'ref/' . $f_ref;
            }
            
            if( trim($f_title) != '' )
            {
                if( $segments != '' ) $segments .= '/';
                $segments .= 'title/' . $f_title;
            }
            
            if( trim($f_category) != '' )
            {
                if( $segments != '' ) $segments .= '/';
                $segments .= 'category/' . $f_category;
            }
            
            if( $segments != '' )
            {
                $segments = $segments . '/';
            }
    
            $pageTitle = 'Inform\'Actuel Admin - Liste des films';
            $folderPath = 'admin/movie_list/';
    
            $cssResources = array(
                array('var_resource' => path_css() . 'style.css'),
                array('var_resource' => path_css() . 'modal.css'),
                array('var_resource' => path_css() . 'tbldata.css'),
                array('var_resource' => path_css() . 'formvalidation.css'),
                array('var_resource' => path_css() . 'simplebutton.css'),
                array('var_resource' => path_css() . 'ui/jquery.ui.all.css'),
            );
            
            $jsResources = array(
                array('var_resource' => path_js() . 'jquery.min.js'),
                array('var_resource' => path_js() . 'jquery.easing-sooper.js'),
                array('var_resource' => path_js() . 'jquery.sooperfish.js'),
                array('var_resource' => path_js() . 'main.js'),
                array('var_resource' => path_js() . 'jquery.form.js'),
                
                array('var_resource' => path_js() . 'ui/jquery-ui.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.core.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.widget.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.button.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.mouse.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.draggable.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.position.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.dialog.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.datepicker.js'),
            );
            
            $jsLiterals = array(
                array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.php', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse('admin/portal/scripts/logout.php', assets_paths(), TRUE)),
            );
            
            /* Setting the header content */
            
            $headerData = assets_paths();
            $headerData = array_merge($headerData, array(
                'modal_confirm_logout' => $this->parser->parse('admin/portal/modal/confirm_logout', assets_paths(), TRUE),
            ));
            
            /* Setting the center content */
            $var_movies = $this->movie_model->fetch_array_movies_by($start, self::COUNT_PER_PAGE, $movie_type, $f_title, $f_ref, $f_category);
            $totalRows = $this->movie_model->count_movies_by($movie_type, $f_title, $f_ref, $f_category);
            
                /* Setting up pagination */    
            $config = array(
                'base_url' => base_url() . 'admin/movie_list/fetch/'. $movie_type . '/' . $segments . 'start/',
                'total_rows' => $totalRows,
                'per_page' => self::COUNT_PER_PAGE,
     			'first_link' => 'D&eacute;but',
    			'last_link' => 'Fin',
    			'next_link' => '&gt;',
    			'prev_link' => '&lt;',
    			'cur_tag_open' => '&nbsp;<strong class="inactive_anchor">',
    			'cur_tag_close' => '</strong>',
                'cur_page' => $start,
            );
            $this->pagination->initialize($config);
            
            $var_pagination = $this->pagination->create_links();
                /* End of setting up pagination */
    
            $centerData = assets_paths();
            $centerData = array_merge($centerData, array(
                'var_movies' => $var_movies,
                'var_pagination' => $var_pagination,
                'var_current_type' => $movie_type,
            ));
            
            /* Setting the footer content */
            $footerData = assets_paths();
            $footerData = array_merge($footerData, array(
            ));
            
            $var_header_zone = $this->parser->parse($folderPath . 'header_zone', $headerData, TRUE);
            $var_center_zone = $this->parser->parse($folderPath . 'center_zone', $centerData, TRUE);
            $var_footer_zone = $this->parser->parse('footer_zone', $footerData, TRUE);
            
            $layoutData = assets_paths();
            $layoutData = array_merge($layoutData, array(
                'var_page_title' => $pageTitle,
                'css_resources' => $cssResources,
                'js_resources' => $jsResources,
                'js_literals' => $jsLiterals,
                'var_header_zone' => $var_header_zone,
                'var_center_zone' => $var_center_zone,
                'var_footer_zone' => $var_footer_zone,
            ));
            
            $this->parser->parse('layout', $layoutData, FALSE);
        }
        else
        {
            redirect('/admin/portal');
        }
    }
}