<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_Model extends CI_Model {
    const TABLE_NAME = 'account';
    
    public function authentify($login, $pwd)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('login', $login)
                        ->where('pwd', sha1($pwd))
                        ->count_all_results();
        
        return ($res == 1);
    }
}