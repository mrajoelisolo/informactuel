/* Modal dialog functions */
function showModalDialog(idDialog)
{
    $("#" + idDialog).modal();
}

function hideModalDialog()
{
    $.modal.close();
}