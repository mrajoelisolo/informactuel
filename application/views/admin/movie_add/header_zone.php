<div id="logo">
    <div id="logo_text">
        <h1>Inform'Actuel</h1>
        <h2>La r&eacute;f&eacute;rence en jeux, logiciels, films et s&eacute;ries TV</h2>
    </div>
</div>
<nav>
    <ul class="sf-menu" id="nav">
        <li><a href="{base_url}admin/homepage">Accueil</a></li>
        <li><a href="{base_url}admin/movie_add">Nouveau film/série</a></li>
        <li><a href="{base_url}admin/movie_list">Liste des films</a></li>
        <li><a href="{base_url}admin/serie_list">Liste des s&eacute;ries</a></li>
    </ul>
</nav>
{modal_confirm_logout}