<script type="text/javascript">
    function logout()
    {
        var idDialog = '#dialog_confirm_logout'; 
        $( idDialog ).dialog({
            resizable: false,
            height:140,
            modal: true,
 			buttons: {
                "Confirmer": function() {
                    $.ajax({
                        url: "{base_url}admin/portal/logout",
                        type: "get",
                        dataType: "json",
                        success: function(res) {
                            document.location = res.url_back;
                        }
                    });
                },
                "Annuler": function() {
                    $( idDialog ).dialog( "close" );
                }
            }
        });
    }
</script>