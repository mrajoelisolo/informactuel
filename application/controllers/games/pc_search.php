<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pc_Search extends CI_Controller
{
    const COUNT_PER_PAGE = 10;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('movie_category_model');
        $this->load->model('game_category_model');
        $this->load->model('game_model');
    }
    
    public function index()
    {
        $this->show();
    }
    
	public function show()
	{
        /* Setting URI segments and filtering */
        $params = $this->uri->uri_to_assoc(4);
        
        $f_keywords  = ( isset($params['kwd'])) ? urldecode($params['kwd']) : '';
        $f_category  = ( isset($params['cat'])) ? urldecode($params['cat']) : '';
        $start = ( isset($params['start'])) ? $params['start'] : 0;
        
        $segments = '';
        if( trim($f_keywords) != '' )
        {
            $segments .= 'kwd/' . $f_keywords;
        }
        
        if( trim($f_category) != '' )
        {
            if( $segments != '' )
                $segments .= '/';

            $segments .= 'cat/' . $f_category;
        }
        
        if( $segments != '' )
        {
            $segments = $segments . '/';
        }
        /* Endof setting URI segments */
       
        $pageTitle = 'Inform\'Actuel - Recherche de jeux PC';
        $folderPath = 'games/pc_search/';
        
        $cssResources = array(
            array('var_resource' => css_url('global')),
            array('var_resource' => css_url('header')),
            array('var_resource' => css_url('slideshow')),
        );
        
        $jsResources = array(
            //array('var_resource' => 'https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js'),
            array('var_resource' => js_url('jquery.min')),
        );
        
        $jsLiterals = array(
            array('var_literal_js' => $this->parser->parse('layout/scripts/slider.tpl', assets_paths(), TRUE)),
            array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.tpl', assets_paths(), TRUE)),
        );
        
        /* Setting datas */
        $var_movie_categories = $this->movie_category_model->fetch_array_categories();
        $var_game_categories = $this->game_category_model->fetch_array_categories();
 
        /* Filtering */
        $movie_title    = $this->input->post('keywords');
        $movie_category = $this->input->post('category');
        
        $var_games = $this->game_model->fetch_array_games_by($start, self::COUNT_PER_PAGE, 'PC', $f_keywords, '', $f_category);
        $totalRows = $this->game_model->count_games_by('PC', $f_keywords, '', $f_category);
        //$var_games = $this->game_model->fetch_catalog('PC', $game_title, $game_category);
        
        /* Setting pagination */
        $config = array(
            'base_url' => base_url() . 'games/pc_search/show/' . $segments . 'start/',
            'total_rows' => $totalRows,
            'per_page' => self::COUNT_PER_PAGE,
            'first_link' => 'D&eacute;but',
            'last_link' => 'Fin',
            'next_link' => '&gt;',
            'prev_link' => '&lt;',
            'cur_tag_open' => '&nbsp;<strong class="inactive_anchor">',
            'cur_tag_close' => '</strong>',
            'cur_page' => $start,
        );
        $this->pagination->initialize($config);
        $var_pagination = $this->pagination->create_links();
        /* End of setting pagination */
 
        /* Header zone */
        $headerData = assets_paths();
        $headerData = array_merge($headerData, array(
            'var_game_categories' => $var_game_categories,
        ));

        /* Slider zone */
        $sliderData = assets_paths();
        $sliderData = array_merge($sliderData, array(
        ));

        /* Left zone */
        $leftData = assets_paths();
        $leftData = array_merge($leftData, array(
            'var_movie_categories' => $var_movie_categories,
            'var_game_categories' => $var_game_categories,
        ));

        /* Center zone */
        $centerData = assets_paths();
        $centerData = array_merge($centerData, array(
            'keywords' => $f_keywords,
            'var_games' => $var_games,
            'var_pagination' => $var_pagination,
        ));
        
        /* Left zone */
        $rightData = assets_paths();
        $rightData = array_merge($rightData, array(
        ));
        
        /* Footer zone */
        $footerData = assets_paths();
        $footerData = array_merge($footerData, array(
        ));
        
        $var_header_zone = $this->parser->parse($folderPath . 'zones/header_zone.tpl', $headerData, TRUE);
        $var_slider_zone = $this->parser->parse('layout/slider_zone.tpl', $sliderData, TRUE);
        $var_left_zone = $this->parser->parse('layout/left_zone.tpl', $leftData, TRUE);
        $var_center_zone = $this->parser->parse($folderPath . 'zones/center_zone.tpl', $centerData, TRUE);
        $var_right_zone = $this->parser->parse('layout/right_zone.tpl', $rightData, TRUE);
        $var_footer_zone = $this->parser->parse('layout/footer_zone.tpl', $footerData, TRUE);
        
        $layoutData = assets_paths();
        $layoutData = array_merge($layoutData, array(
            'var_page_title' => $pageTitle,
            'css_resources' => $cssResources,
            'js_resources' => $jsResources,
            'js_literals' => $jsLiterals,

            'var_header_zone' => $var_header_zone,
            'var_slider_zone' => $var_slider_zone,
            'var_left_zone' => $var_left_zone,
            'var_center_zone' => $var_center_zone,
            'var_right_zone' => $var_right_zone,
            'var_footer_zone' => $var_footer_zone,
        ));
        
        $this->parser->parse('layout/layout.tpl', $layoutData, FALSE);
	}
}

/* End of file pc_search.php */
/* Location: ./application/controllers/pc_search.php */