<div id="sidebar_container">
    <div class="sidebar">
        <h3></h3>
        <div class="sidebar_item">
            <p>
            <input class="button purple" type="button" value="Fermer la session" onclick="logout();">
        </p>
        </div>
    </div>
    
    <div class="sidebar">
        <h3>Messages</h3>
        <div class="sidebar_item">
            <p><strong>Statistique sur les références incorrects</strong></p>
            <p><strong>Jeux PC: </strong>{var_count_incorrect_game_pc_ref}</p>
            <p><strong>Jeux PS2: </strong>{var_count_incorrect_game_ps2_ref}</p>
            <p><strong>Films : </strong>{var_count_incorrect_movie_ref}</p>
            <p><strong>Séries : </strong>{var_count_incorrect_serie_ref}</p>
        </div>
    </div>
</div>
<div class="content">
    <h1>Que voulez vous faire ?</h1>
    <table class="tbl-data">
        <thead>
            <tr>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><input class="button green" type="button" onclick="manage_games();" value="G&eacute;rer la liste des jeux"></td>
            </tr>
            <tr>
                <td><input class="button blue" type="button" onclick="manage_softs();" value="G&eacute;rer la liste des logiciels"></td>
            </tr>
            <tr>
                <td><input class="button purple" type="button" onclick="manage_movies();" value="G&eacute;rer la liste des films et s&eacute;ries"></td>
            </tr>
        </tbody>
    </table>
    <br><br>
</div>