<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie_Category_Model extends CI_Model
{
    const TABLE_NAME = 'movie_category';
    
    public function fetch_array_categories()
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->order_by('category_name', 'asc')
                        ->get()
                        ->result_array();
        
        return $res;
    }
    
    public function fetch_category_by_id($id)
    {
        $res = $this->db->select('*')
                    ->from(self::TABLE_NAME)
                    ->where('category_id', $id)
                    ->get()
                    ->row_array();
        return $res;
    }
}