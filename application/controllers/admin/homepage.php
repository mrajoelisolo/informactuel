<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('stat_model');
    }
    
    public function index()
    {
        if($this->session->userdata('user_login'))
        {
            $pageTitle = 'Inform\'Actuel Admin - Accueil';
            $folderPath = 'admin/homepage/';
    
            $cssResources = array(
                array('var_resource' => path_css() . 'style.css'),
                array('var_resource' => path_css() . 'modal.css'),
                array('var_resource' => path_css() . 'tbldata.css'),
                array('var_resource' => path_css() . 'simplebutton.css'),
            );
            
            $jsResources = array(
                array('var_resource' => path_js() . 'jquery.min.js'),
                array('var_resource' => path_js() . 'jquery.easing-sooper.js'),
                array('var_resource' => path_js() . 'jquery.sooperfish.js'),
                array('var_resource' => path_js() . 'main.js'),
            );
            
            $jsLiterals = array(
                array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.php', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse('admin/portal/scripts/logout.php', assets_paths(), TRUE)),
            );
            
            /* Setting datas */
            $var_count_incorrect_game_pc_ref = $this->stat_model->countIncorrectGameReferences('PC');
            $var_count_incorrect_game_ps2_ref = $this->stat_model->countIncorrectGameReferences('PS2');
            
            $var_count_incorrect_movie_ref = $this->stat_model->countIncorrectMovieReferences();
            $var_count_incorrect_serie_ref = $this->stat_model->countIncorrectSerieReferences();
            
            /* Setting the header content */
            $headerData = assets_paths();
            $headerData = array_merge($headerData, array(
            ));
            
            /* Setting the center content */
            $centerData = assets_paths();
            $centerData = array_merge($centerData, array(
                'var_count_incorrect_game_pc_ref' => $var_count_incorrect_game_pc_ref,
                'var_count_incorrect_game_ps2_ref' => $var_count_incorrect_game_ps2_ref,
                'var_count_incorrect_movie_ref' => $var_count_incorrect_movie_ref,
                'var_count_incorrect_serie_ref' => $var_count_incorrect_serie_ref,
            ));
            
            /* Setting the footer content */
            $footerData = assets_paths();
            $footerData = array_merge($footerData, array(
            ));
            
            $var_header_zone = $this->parser->parse($folderPath . 'header_zone', $headerData, TRUE);
            $var_center_zone = $this->parser->parse($folderPath . 'center_zone', $centerData, TRUE);
            $var_footer_zone = $this->parser->parse('footer_zone', $footerData, TRUE);
            
            $layoutData = assets_paths();
            $layoutData = array_merge($layoutData, array(
                'var_page_title' => $pageTitle,
                'css_resources' => $cssResources,
                'js_resources' => $jsResources,
                'js_literals' => $jsLiterals,
                'var_header_zone' => $var_header_zone,
                'var_center_zone' => $var_center_zone,
                'var_footer_zone' => $var_footer_zone,
            ));
            
            $this->parser->parse('layout', $layoutData, FALSE);
                }
        else
        {
            redirect('/admin/portal');
        }
    }
}