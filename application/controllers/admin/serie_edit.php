<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Serie_Edit extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('movie_model');
        $this->load->model('movie_category_model');
        $this->load->model('movie_type_model');
    }
    
    public function edit($movie_ref) {
        if($this->session->userdata('user_login'))
        {
            $pageTitle = 'Inform\'Actuel Admin - Edition de série';
            $folderPath = 'admin/serie_edit/';
    
            $cssResources = array(
                array('var_resource' => path_css() . 'style.css'),
                array('var_resource' => path_css() . 'modal.css'),
                array('var_resource' => path_css() . 'tbldata.css'),
                array('var_resource' => path_css() . 'formvalidation.css'),
                array('var_resource' => path_css() . 'simplebutton.css'),
                
                array('var_resource' => path_css() . 'ui/jquery.ui.all.css'),
            );
            
            $jsResources = array(
                array('var_resource' => path_js() . 'jquery.min.js'),
                array('var_resource' => path_js() . 'jquery.easing-sooper.js'),
                array('var_resource' => path_js() . 'jquery.sooperfish.js'),
                array('var_resource' => path_js() . 'main.js'),
                array('var_resource' => path_js() . 'jquery.form.js'),
                array('var_resource' => path_js() . 'formvalidation.js'),
                
                array('var_resource' => path_js() . 'ui/jquery-ui.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.core.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.widget.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.button.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.mouse.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.draggable.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.position.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.dialog.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.datepicker.js'),
            );
            
            $jsLiterals = array(
                array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.php', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse('admin/portal/scripts/logout.php', assets_paths(), TRUE)),
            );
            
            /* Setting the header content */
            
            $headerData = assets_paths();
            $headerData = array_merge($headerData, array(
                'modal_confirm_logout' => $this->parser->parse('admin/portal/modal/confirm_logout', assets_paths(), TRUE),
            ));
            
            /* Setting the center content */
            $var_movie_categories = $this->movie_category_model->fetch_array_categories();
            $var_movie_types = $this->movie_type_model->get_array_movie_types();
            $arr_selected_movie = $this->movie_model->fetch_array_one_movie_by_ref($movie_ref);
                    
            for( $i = 0; $i < count($var_movie_categories); $i++ )
            {
                if( $var_movie_categories[$i]['category_name'] == $arr_selected_movie['category_name'] )
                {
                    $var_movie_categories[$i]['selected_movie_category'] = 'selected="selected"';
                }
                else
                {
                    $var_movie_categories[$i]['selected_movie_category'] = '';
                }
            }
            
            for( $i = 0; $i < count($var_movie_types); $i++ )
            {
                if( $var_movie_types[$i]['type_name'] == $arr_selected_movie['movie_type'] )
                {
                    $var_movie_types[$i]['selected_movie_type'] = 'selected="selected"';
                }
                else
                {
                    $var_movie_types[$i]['selected_movie_type'] = '';
                }
            }
                    
            $centerData = assets_paths();
            $centerData = array_merge($centerData, array(
                'var_movie_categories' => $var_movie_categories,
                'var_movie_types' => $var_movie_types,
                'var_movie_title' => $arr_selected_movie['movie_title'],
                'var_movie_ref' => $arr_selected_movie['movie_ref'],
                'var_movie_description' => $arr_selected_movie['movie_description'],
                'var_movie_preview' => htmlentities($arr_selected_movie['movie_preview'], ENT_QUOTES, 'UTF-8'),
                'var_dvd_checked' => ($arr_selected_movie['movie_dvd'] == 1) ? 'checked="checked"' : "" ,
                'var_cd_checked' => ($arr_selected_movie['movie_cd'] == 1) ? 'checked="checked"' : "" ,
                'var_movie_actors_tag' => $arr_selected_movie['movie_actors_tag'],
                'var_movie_director' => $arr_selected_movie['movie_director'],
                'var_movie_release_year' => $arr_selected_movie['movie_release_year'],
                'var_movie_duration_minute' => $arr_selected_movie['movie_duration_minute'],
                'var_movie_url' => $arr_selected_movie['movie_url'],
            ));
            
            /* Setting the footer content */
            $footerData = assets_paths();
            $footerData = array_merge($footerData, array(
            ));
            
            $var_header_zone = $this->parser->parse($folderPath . 'header_zone', $headerData, TRUE);
            $var_center_zone = $this->parser->parse($folderPath . 'center_zone', $centerData, TRUE);
            $var_footer_zone = $this->parser->parse('footer_zone', $footerData, TRUE);
            
            $layoutData = assets_paths();
            $layoutData = array_merge($layoutData, array(
                'var_page_title' => $pageTitle,
                'css_resources' => $cssResources,
                'js_resources' => $jsResources,
                'js_literals' => $jsLiterals,
                'var_header_zone' => $var_header_zone,
                'var_center_zone' => $var_center_zone,
                'var_footer_zone' => $var_footer_zone,
            ));
            
            $this->parser->parse('layout', $layoutData, FALSE);
        }
        else
        {
            redirect('/admin/portal');
        }
    }

    public function update()
    {
        $movie_old_ref     = $this->input->post('movie_old_ref');
        $movie_title       = $this->input->post('movie_title');
        $movie_ref         = $this->input->post('movie_ref');
        $movie_description = $this->input->post('movie_description');
        $movie_type        = $this->input->post('movie_type');
        $movie_preview     = html_entity_decode($this->input->post('movie_preview'), ENT_QUOTES, 'UTF-8');
        $movie_dvd         = ($this->input->post('movie_dvd')) ? 1 : 0;
        $movie_cd          = ($this->input->post('movie_cd')) ? 1 : 0;
        $movie_actors_tag  = $this->input->post('movie_actors_tag');
        $category_fk       = $this->input->post('movie_category');
        $movie_director        = $this->input->post('movie_director');
        $movie_release_year    = intval($this->input->post('movie_release_year'));
        $movie_duration_minute = intval($this->input->post('movie_duration_minute'));
        $movie_url         = $this->input->post('movie_url');
        
        $ref_exists = $this->movie_model->check_if_ref_exists($movie_ref);
        if( $ref_exists )
        {
            $response = array(
                'error'   => 1,
                'message' => 'La r&eacute;f&eacute;rence &eacute;xiste d&eacute;ja',
            );
            
            echo json_encode($response);
            return;
        }
        
        $filename = NULL;
        $response = array(
            'error'   => 0,
            'movie_ref' => $movie_ref,
            'movie_type' => $movie_type,
            'message' => 'Mise a jour effectu&eacute;e',
        );
        
        if( isset($_FILES['movie_file_image']) )
        {   
            /* Remove first the ancient file */
            $arr_old_movie = $this->movie_model->fetch_array_one_movie_by_ref($movie_old_ref);
            $old_filename = $arr_old_movie['movie_thumbnail1'];
            
            if( ! empty($old_filename) )
            {
                $old_filepath = './assets/images/movies/thumbnails/' . $old_filename; 
                if( file_exists($old_filepath) )
                {
                    unlink($old_filepath);
                }
            }            
            
            $config = array(
                'upload_path'   => './assets/images/movies/thumbnails/',
                'allowed_types' => 'png|jpg|gif',
                'file_name'     => 'movie_' . strtolower($movie_type) . '_' . sha1($_FILES['movie_file_image']['name']), 
            );
            
            $this->upload->initialize($config);
            
            if( ! $this->upload->do_upload('movie_file_image') )
            {
                $error = strip_tags($this->upload->display_errors());
                
                $response['error'] = 1;
                $response['message'] = $error;
            }
            else
            {
                $fileInfo = array(
                    'upload_data' => $this->upload->data(),
                );
                
                $filename = $fileInfo['upload_data']['file_name'];
                $img_url = base_url() . 'assets/images/movies/thumbnails/' . $filename;

                $response['error'] = 0;
                $response['image'] = $img_url;
                
                //Resize the image
                $imgConfig = array(
                    'image_library'  => 'gd2',
                    'source_image'   => './assets/images/movies/thumbnails/' . $filename,
                    'maintain_ratio' => TRUE,
                    'width'          => 256,
                    'height'         => 144,
                );
                
                $this->image_lib->initialize($imgConfig);
                $this->image_lib->resize();
            }
        }
        
        //Insert the movie info to the database
        if( $response['error'] == 0 ) {
            $this->movie_model->update_movie_by_ref($movie_title, $movie_ref, $movie_old_ref, $movie_description, $movie_type, $movie_preview, $filename,
                $movie_dvd, $movie_cd, $movie_actors_tag, $category_fk, $movie_director, $movie_release_year, $movie_duration_minute, $movie_url
            );
        }
        
        echo json_encode($response);
    }
    
    public function delete()
    {
        $movie_ref = $this->input->post('ref');
        
        $this->movie_model->delete_movie_by_ref($movie_ref);
        
        $response = array (
            'error' => 0,
        );
        
        echo json_encode($response);
    }
}