<div id="sidebar_container">
    <div class="sidebar">
        <h3>Actions</h3>
        <div class="sidebar_item">
            <p>
                <input class="button purple" type="button" value="Fermer la session" onclick="logout();">
            </p>
        </div>
    </div>
</div>
<div class="content">
    <h1>Liste des logiciels {var_current_platform}</h1>
    
    <div class="content_item">
        <form id="form_search" method="post" action="#">
            <input type="hidden" id="txt_current_platform" value="{var_current_platform}">
            <p>
                <label for="soft_ref"><strong>R&eacute;f&eacute;rence du logiciel: </strong></label>
                <input type="text" name="txt_soft_ref" id="txt_soft_ref">
                <label for="soft_ref"><strong>Titre du logiciel: </strong></label>
                <input type="text" name="txt_soft_title" id="txt_soft_title">
            </p>
            <p>
                <input class="button darkblue" type="submit" value="Rechercher">
            </p>
        </form>
    </div>
    
    <div class="wrap-pagination">
        {var_pagination}
    </div>
    
    <table class="tbl-data">
        <thead>
            <tr>
                <th>R&eacute;f&eacute;rence</th>
                <th>Titre</th>
                <th>Cat&eacute;gorie</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            {var_softs}
            <tr id="{soft_ref}">
                <td>{soft_ref}</td>
                <td>{soft_title}</td>
                <td>{category_name}</td>
                <td>
                    <input class="button green" type="button" value="Modifier" onclick="edit_soft('{soft_ref}');">
                    <input class="button orange" type="button" value="Supprimer" onclick="delete_soft('{soft_ref}');">
                </td>
            </tr>
            {/var_softs}
        </tbody>
    </table>
    
    <div class="wrap-pagination">
        {var_pagination}
    </div>
</div>