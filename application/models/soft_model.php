<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Soft_Model extends CI_Model
{
    const TABLE_NAME = 'soft';
    const TABLE_CATEGORY = 'soft_category';
    
    public function fetch_array_latest_softs($soft_platform, $start = 0, $count = 5)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                        ->where('g.category_fk = c.category_id')
                        ->where('soft_platform', $soft_platform)
                        ->order_by('soft_ref', 'desc')
                        ->limit($count, $start)
                        ->get()
                        ->result_array();
        
        return $res;
    }
    
    public function count_all_softs_by_platform($soft_platform)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('soft_platform', $soft_platform)
                        ->count_all_results();
        return $res;
    }
    
    public function fetch_catalog($soft_platform, $soft_title, $category_name)
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.soft_platform', $soft_platform);
        
        if( trim($soft_title) != '' )
            $this->db->like('g.soft_title', $soft_title, 'both');
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
        
        $res = $this->db->order_by('g.soft_title', 'asc')
                        ->get()
                        ->result_array();
                        
        return $res;
    }
    
    public function save_soft($soft_title, $soft_ref, $soft_description, $soft_platform, $soft_thumbnail,
        $soft_link, $soft_config_min, $soft_dvd, $soft_cd, $category_fk)
    {
        $this->db->set('soft_title', $soft_title);
        $this->db->set('soft_ref', $soft_ref);
        $this->db->set('soft_description', $soft_description);
        $this->db->set('soft_platform', $soft_platform);
        $this->db->set('soft_thumbnail1', $soft_thumbnail);
        $this->db->set('soft_link', $soft_link);
        $this->db->set('soft_config_min', $soft_config_min);
        $this->db->set('soft_dvd', $soft_dvd);
        $this->db->set('soft_cd', $soft_cd);
        $this->db->set('category_fk', $category_fk);
        $this->db->insert(self::TABLE_NAME);
    }
    
    public function check_if_ref_exists($soft_ref)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('soft_ref', $soft_ref)
                        ->count_all_results();
        
        return ($res > 0);
    }
    
    public function fetch_array_softs_by($start = 0, $count = 5, $soft_platform, $soft_title = '', $soft_ref = '',$category_name = '', $order_ref_by = 'desc')
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.soft_platform', $soft_platform);
                 
        if( trim($soft_title) != '' )
            $this->db->like('g.soft_title', $soft_title, 'both');
        
        if( trim($soft_ref) != '' )
            $this->db->where('g.soft_ref', $soft_ref);
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
                        
        $res = $this->db->order_by('g.soft_ref', $order_ref_by)
                        ->limit($count, $start)
                        ->get()
                        ->result_array();
        return $res;
    }
    
    public function count_softs_by($soft_platform, $soft_title = '', $soft_ref = '', $category_name = '')
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.soft_platform', $soft_platform);
        
        if( trim($soft_title) != '' )
            $this->db->like('g.soft_title', $soft_title, 'both');
            
        if( trim($soft_ref) != '' )
            $this->db->where('g.soft_ref', $soft_ref);
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
            
        $res = $this->db->count_all_results();
        
        return $res;
    }
    
    public function fetch_array_one_soft_by_ref($soft_ref) {
        $res = $this->db->select('*')
                    ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                    ->where('g.category_fk = c.category_id')
                    ->where('g.soft_ref', $soft_ref)
                    ->get()
                    ->row_array();
                 
        return $res;
    }

    public function fetch_array_one_last_soft_by_platform($platform)
    {
        $res = $this->db->query('SELECT * FROM ' . self::TABLE_NAME . ' g
                                WHERE soft_platform = ?
                                AND g.soft_ref = (SELECT MAX(soft_ref) FROM ' . self::TABLE_NAME . '
                                WHERE soft_platform = ?);', array(
                                    $platform, $platform
                                ))
                        ->row_array();
                 
        return $res;
    }

    public function update_soft_by_ref($soft_ref, $soft_old_ref, $soft_title, $soft_description, $soft_platform, $soft_thumbnail,
        $soft_link, $soft_config_min, $soft_dvd, $soft_cd, $category_fk)
    {
        $data = array(
            'soft_ref' => $soft_ref,
            'soft_title' => $soft_title,
            'soft_description' => $soft_description,
            'soft_platform' => $soft_platform,
            'soft_link' => $soft_link,
            'soft_config_min' => $soft_config_min,
            'soft_dvd' => $soft_dvd,
            'soft_cd' => $soft_cd,
            'category_fk' => $category_fk,
        );
        
        if( ! empty($soft_thumbnail) )
        {
            $data['soft_thumbnail1'] = $soft_thumbnail;
        }
        
        $this->db->where('soft_ref', $soft_old_ref);
        $this->db->update(self::TABLE_NAME, $data);
    }
    
    public function delete_soft_by_ref($soft_ref)
    {
        $this->db->where('soft_ref', $soft_ref);
        $this->db->delete(self::TABLE_NAME);
    }
}