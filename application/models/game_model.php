<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_Model extends CI_Model
{
    const TABLE_NAME = 'game';
    const TABLE_CATEGORY = 'game_category';
    
    public function fetch_array_latest_games($game_platform, $start = 0, $count = 5)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                        ->where('g.category_fk = c.category_id')
                        ->where('game_platform', $game_platform)
                        ->order_by('game_ref', 'desc')
                        ->limit($count, $start)
                        ->get()
                        ->result_array();
        return $res;
    }
    
    public function count_all_games_by_platform($game_platform)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('game_platform', $game_platform)
                        ->count_all_results();
        return $res;
    }
    
    public function fetch_catalog($game_platform, $game_title, $category_name)
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.game_platform', $game_platform);
        
        if( trim($game_title) != '' )
            $this->db->like('g.game_title', $game_title, 'both');
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
        
        $res = $this->db->order_by('g.game_title', 'asc')
                        ->get()
                        ->result_array();
                        
        return $res;
    }
    
    public function fetch_array_games_by($start = 0, $count = 5, $game_platform, $game_title = '', $game_ref = '',$category_name = '', $order_ref_by = 'desc')
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.game_platform', $game_platform);
                 
        if( trim($game_title) != '' )
            $this->db->like('g.game_title', $game_title, 'both');
        
        if( trim($game_ref) != '' )
            $this->db->where('g.game_ref', $game_ref);
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
                        
        $res = $this->db->order_by('g.game_ref', $order_ref_by)
                        ->limit($count, $start)
                        ->get()
                        ->result_array();
        return $res;
    }
    
    public function count_games_by($game_platform, $game_title = '', $game_ref = '', $category_name = '')
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.game_platform', $game_platform);
        
        if( trim($game_title) != '' )
            $this->db->like('g.game_title', $game_title, 'both');
            
        if( trim($game_ref) != '' )
            $this->db->where('g.game_ref', $game_ref);
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
            
        $res = $this->db->count_all_results();
        
        return $res;
    }
    
    public function fetch_array_one_game_by_ref($game_ref) {
        $res = $this->db->select('*')
                    ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                    ->where('g.category_fk = c.category_id')
                    ->where('g.game_ref', $game_ref)
                    ->get()
                    ->row_array();
                 
        return $res;
    }
    
    public function fetch_array_one_last_game_by_platform($platform)
    {
        $res = $this->db->query('SELECT * FROM ' . self::TABLE_NAME . ' g
                                WHERE game_platform = ?
                                AND g.game_ref = (SELECT MAX(game_ref) FROM ' . self::TABLE_NAME . '
                                WHERE game_platform = ?);', array(
                                    $platform, $platform
                                ))
                        ->row_array();
                 
        return $res;
    }
    
    public function save_game($game_title, $game_ref, $game_description, $game_platform, $game_thumbnail,
        $game_link, $game_config_min, $game_dvd, $game_cd, $category_fk)
    {
        $this->db->set('game_title', $game_title);
        $this->db->set('game_ref', $game_ref);
        $this->db->set('game_description', $game_description);
        $this->db->set('game_platform', $game_platform);
        $this->db->set('game_thumbnail1', $game_thumbnail);
        $this->db->set('game_link', $game_link);
        $this->db->set('game_config_min', $game_config_min);
        $this->db->set('game_dvd', $game_dvd);
        $this->db->set('game_cd', $game_cd);
        $this->db->set('category_fk', $category_fk);
        $this->db->insert(self::TABLE_NAME);
    }
    
    public function update_game_by_ref($game_ref, $game_old_ref, $game_title, $game_description, $game_platform, $game_thumbnail,
        $game_link, $game_config_min, $game_dvd, $game_cd, $category_fk)
    {
        $data = array(
            'game_ref' => $game_ref,
            'game_title' => $game_title,
            'game_description' => $game_description,
            'game_platform' => $game_platform,
            'game_link' => $game_link,
            'game_config_min' => $game_config_min,
            'game_dvd' => $game_dvd,
            'game_cd' => $game_cd,
            'category_fk' => $category_fk,
        );
        
        if( ! empty($game_thumbnail) )
        {
            $data['game_thumbnail1'] = $game_thumbnail;
        }
        
        $this->db->where('game_ref', $game_old_ref);
        $this->db->update(self::TABLE_NAME, $data);
    }
    
    public function delete_game_by_ref($game_ref)
    {
        $this->db->where('game_ref', $game_ref);
        $this->db->delete(self::TABLE_NAME);
    }
    
    public function check_if_ref_exists($game_ref)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('game_ref', $game_ref)
                        ->count_all_results();
        
        return ($res > 0);
    }
}