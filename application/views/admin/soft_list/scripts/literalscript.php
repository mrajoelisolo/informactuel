<script type="text/javascript">
    $('#form_search').submit(function(e) {
        e.preventDefault();
        
        var currentPlatform = $('#txt_current_platform').val();
        
        var f_soft_ref = $('#txt_soft_ref').val();
        var f_soft_title = $('#txt_soft_title').val();
        
        var criteria = '';
        if( f_soft_ref != '' )
            criteria += 'ref/' +  f_soft_ref;
            
        if( f_soft_title != '' ) {
            if(criteria != '')
                criteria += '/';    
            
            criteria += 'title/' +  f_soft_title;
        }
        
        document.location = '{base_url}/admin/soft_list/fetch/' + currentPlatform + '/' + criteria;
    });
    
    function delete_soft(soft_ref)
    {
        var idDialog = '#dialog_confirm_remove_soft'; 
        $( idDialog ).dialog({
            resizable: false,
            height:140,
            modal: true,
 			buttons: {
                "Confirmer": function() {
                    $.ajax({
                        url: "{base_url}admin/soft_edit/delete",
                        type: "post",
                        dataType: "json",
                        data: { ref: soft_ref },
                        success: function(res) {
                            $( idDialog ).dialog( "close" );
    
                            if( res.error == 0 )
                            {
                                var row = $('#' + soft_ref);
                                $(row).remove();
                            }
                        }
                    });
                },
                "Annuler": function() {
                    $( idDialog ).dialog( "close" );
                }
            }
        });
    }
    
    function edit_soft(soft_ref)
    {
        document.location = '{base_url}admin/soft_edit/edit/' + soft_ref;
    }
    
    $(function() {

    });
</script>