<div id="sidebar_container">
    <div class="sidebar">
        <h3>Actions</h3>
        <div class="sidebar_item">
            <p>
                <input class="button purple" type="button" value="Fermer la session" onclick="logout();">
            </p>
        </div>
    </div>
</div>
<div class="content">
    <h1>Edition de série</h1>
    <form id="form-movie" action="{base_url}admin/serie_edit/update" enctype="multipart/form-data" method="post">
        <input name="MAX_FILE_SIZE" type="hidden" value="1000000">
        <input type="hidden" name="movie_old_ref" value="{var_movie_ref}">
        <table class="tbl-data">
            <thead>
                <tr>
                    <th colspan="2">General</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="col1"><strong>Movie title *</strong></td>
                    <td class="col2"><input type="text" id="movie_title" name="movie_title" class="input-txt" value="{var_movie_title}"></td>
                </tr>
                
                <tr>
                    <td><strong>Movie reference *</strong></td>
                    <td><input type="text" id="movie_ref" name="movie_ref" value="{var_movie_ref}"></td>
                </tr>
                
                <tr>
                    <td><strong>Movie category</strong></td>
                    <td>
                        <select id="movie_category" class="select-cmb" name="movie_category">
                            {var_movie_categories}
                            <option {selected_movie_category} value="{category_id}">{category_name}</option>
                            {/var_movie_categories}
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td><strong>Directed by</strong></td>
                    <td><input type="text" id="movie_director" name="movie_director" class="input-txt" value="{var_movie_director}"></td>
                </tr>
                
                <tr>
                    <td><strong>Release year</strong></td>
                    <td><input type="tex" id="movie_release_year" name="movie_release_year" value="{var_movie_release_year}"></td>
                </tr>
                
                <tr>
                    <td><strong>Duration in minutes</strong></td>
                    <td><input type="text" id="movie_duration_minute" name="movie_duration_minute" value="{var_movie_duration_minute}"></td>
                </tr>
                
                <tr>
                    <td><strong>Movie description</strong></td>
                    <td><textarea id="movie_description" name="movie_description">{var_movie_description}</textarea></td>
                </tr>
                
                <tr>
                    <td><strong>Type</strong></td>
                    <td>
                        <select id="movie_type" class="select-cmb" name="movie_type">
                            {var_movie_types}
                            <option {selected_movie_type} value="{type_name}">{type_name}</option>
                            {/var_movie_types}
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td><strong>Movie integration</strong></td>
                    <td><input class="input-txt" type="text" id="movie_preview" name="movie_preview" value="{var_movie_preview}"></td>
                </tr>
                
                <tr>
                    <td><strong>Movie cover</strong></td>
                    <td><input type="file" id="movie_file_image" name="movie_file_image" accept="image/*"></td>
                </tr>
                
                <tr>
                    <td><strong>Actors tag (separate with comma)</strong></td>
                    <td><input class="input-txt" type="text" id="movie_actors_tag" name="movie_actors_tag" value="{var_movie_actors_tag}"></td>
                </tr>

                <tr>
                    <td><strong>Movie url</strong></td>
                    <td><input class="input-txt" type="text" id="movie_url" name="movie_url" value="{var_movie_url}"></td>
                </tr>

                <tr>
                    <td><strong>Supports</strong></td>
                    <td>
                        <input type="checkbox" id="movie_dvd" name="movie_dvd" value="movie_dvd" {var_dvd_checked}> DVD<br>
                        <input type="checkbox" id="movie_cd" name="movie_cd" value="movie_cd" {var_cd_checked}> CD
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Mettre à jour" class="button green">
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="div-message">
            <img id="ajax-loading" src="{path_img}ajax-loader.gif" style="display:none;">
            <span id="ajax-message"></span>
        </div>
    </form>
</div>