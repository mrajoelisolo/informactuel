<div class="menubar">
    <div id="menu">
   	    <ul>
       	    <li><a href="{base_url}">accueil</a></li>
            <li><a href="#">logiciels</a>
           	    <ul class="sousmenu">
               	    <li><a href="{base_url}softs/pc_list">pc</a></li>
                    <li><a href="#">mac</a></li>
                </ul>
            </li>
            <li><a href="#" class="activepage">films</a></li>
            <li><a href="{base_url}series/serie_list">séries tv</a></li>
            <li><a href="#">jeux</a>
           	    <ul class="sousmenu">
               	    <li><a href="{base_url}games/pc_list">pc</a></li>
                    <li><a href="#">playstation 1</a></li>
                    <li><a href="#">playstation 2</a></li>
                    <li><a href="#">playstation 3</a></li>
                    <li><a href="#">XBox</a></li>
                    <li><a href="#">XBox 360</a></li>
                </ul>
            </li>
        </ul>
    </div>            
</div>

<!-- //////////////////////////////////////// FIN BARRE DE MENU //////////////////////////////////////// -->

<div class="headerbg">
    <div class="txtlogo"></div>
</div>

<!-- //////////////////////////////////////// ZONE DE RECHERCHE //////////////////////////////////////// -->

<div class="searchbar">
    <form method="post" action="#" id="form-search">
        <div class="searchform1">
                <p>
                    <label for="recherche">Rechercher</label>
                    <input type="text" name="keywords" id="keywords" placeholder="Tous" size="25" maxlength="100" />
                </p>
       	</div>
        <div class="searchform2">
                <p>
                    <select name="category" id="category">
                        <option value="">Tout</option>
                        {var_movie_categories}
                        <option value="{category_name}">{category_name}</option>
                        {/var_movie_categories}
                    </select>
                </p>        
        </div>
        <div>
       	    <input type="submit" class="searchbt" value="" />
        </div>
    </form>


    <div class="spacer"></div>

<!-- //////////////////////////////////////// FIN ZONE DE RECHERCHE //////////////////////////////////////// -->
                
</div>
<div class="graphiclogo">
    <a href="#"><img src="{path_img}graphic_logo.png" alt="Inform'Actuel" title="Inform'Actuel" /></a>
</div>