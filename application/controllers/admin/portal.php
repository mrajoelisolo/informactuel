<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('account_model');
    }
    
    public function index()
    {
        $pageTitle = 'Inform\'Actuel Admin - Authentification';
        $folderPath = 'admin/portal/';

        $cssResources = array(
            array('var_resource' => path_css() . 'login.css'),
            array('var_resource' => path_css() . 'modal.css'),
            array('var_resource' => path_css() . 'tbldata.css'),
            array('var_resource' => path_css() . 'formvalidation.css'),
            array('var_resource' => path_css() . 'simplebutton.css'),
        );
        
        $jsResources = array(
            array('var_resource' => path_js() . 'jquery.min.js'),
            array('var_resource' => path_js() . 'jquery.easing-sooper.js'),
            array('var_resource' => path_js() . 'jquery.sooperfish.js'),
            array('var_resource' => path_js() . 'main.js'),
            array('var_resource' => path_js() . 'jquery.form.js'),
        );
        
        $jsLiterals = array(
            array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.php', assets_paths(), TRUE)),
        );
        
        /* Setting the center content */
        $centerData = assets_paths();
        $centerData = array_merge($centerData, array(
        ));
        
        $var_center_zone = $this->parser->parse($folderPath . 'center_zone', $centerData, TRUE);
        
        $layoutData = assets_paths();
        $layoutData = array_merge($layoutData, array(
            'var_page_title' => $pageTitle,
            'css_resources' => $cssResources,
            'js_resources' => $jsResources,
            'js_literals' => $jsLiterals,
            'var_center_zone' => $var_center_zone,
        ));
        
        $this->parser->parse('admin/layout', $layoutData, FALSE);
    }
    
    public function authentify()
    {
        $login = $this->input->post('txt_login');
        $pwd = $this->input->post('txt_pwd');
        
        $res = $this->account_model->authentify($login, $pwd);
        
        if( $res ) 
        {
            $this->session->set_userdata('user_login', $login);
            $this->session->set_userdata('user_pwd', $pwd);
        }
        
        $response = array(
            'logged' => ($res) ? 1 : 0,
            'msg' => ($res) ? '' : 'Login/mot de passe incorrect',
            'success_url' => ($res) ? base_url() . 'admin/homepage' : '#',
        );
        
        echo json_encode($response);
    }
    
    public function logout()
    {
        $this->session->sess_destroy();
        
        $response = array(
            'url_back' => base_url() . 'admin/portal',
        );
        
        echo json_encode($response);
    }
}