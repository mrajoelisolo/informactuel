<div id="logo">
    <div id="logo_text">
        <h1>Inform'Actuel</h1>
        <h2>La r&eacute;f&eacute;rence en jeux, logiciels, films et s&eacute;ries TV</h2>
    </div>
</div>
<nav>
    <ul class="sf-menu" id="nav">
        <li><a href="{base_url}admin/homepage">Accueil</a></li>
        <li><a href="{base_url}admin/soft_add">Nouveau logiciel</a></li>
        <li><a href="{base_url}admin/soft_list/fetch/pc">Liste des logiciels PC</a></li>
        <li><a href="{base_url}admin/soft_list/fetch/mac">Liste des logiciels Mac</a></li>
    </ul>
</nav>
{modal_confirm_logout}

<div id="dialog_confirm_remove_soft" title="Suppression d'un jeu" style="display: none;">
    <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Etes vous sur de vouloir supprimer le logiciel ?</p>
</div>