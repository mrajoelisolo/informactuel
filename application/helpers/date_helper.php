<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('mysqldate_to_str'))
{
	function mysqldate_to_str($strDate)
	{	
		$arr = explode('-', $strDate);
        
        return $arr[2] . '/' . $arr[1] . '/' . $arr[0]; 
	}
}

if( ! function_exists('str_to_mysqldate') )
{
    function str_to_mysqldate($strDate)
    {
        $arr = explode('/', $strDate);
        
        $res = $arr[2] . '-' . $arr[1] . '-' . $arr[0]; 
        
        return $res;
    }
}

?>