<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PC_List extends CI_Controller
{
    const COUNT_PER_PAGE = 10;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('movie_category_model');
        $this->load->model('game_category_model');
        $this->load->model('game_model');
    }
    
    public function index()
    {
        $this->page();
    }
    
	public function page($start = 0)
	{
        $pageTitle = 'Inform\'Actuel - Liste des jeux PC';
        $folderPath = 'games/pc_list/';
        
        $cssResources = array(
            array('var_resource' => css_url('global')),
            array('var_resource' => css_url('header')),
            array('var_resource' => css_url('slideshow')),
        );
        
        $jsResources = array(
            //array('var_resource' => 'https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js'),
            array('var_resource' => js_url('jquery.min')),
        );
        
        $jsLiterals = array(
            array('var_literal_js' => $this->parser->parse('layout/scripts/slider.tpl', assets_paths(), TRUE)),
            array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.tpl', assets_paths(), TRUE)),
        );
        
        /* Setting datas */
        $var_movie_categories = $this->movie_category_model->fetch_array_categories();
        $var_game_categories = $this->game_category_model->fetch_array_categories();
        
        $var_games = $this->game_model->fetch_array_latest_games('PC', $start, self::COUNT_PER_PAGE);
        $totalRows = $this->game_model->count_all_games_by_platform('PC');
        for( $i = 0; $i < count($var_games); $i++)
        {
            $var_games[$i]['var_game_thumbnail'] = path_img() . 'games/thumbnails/' . $var_games[$i]['game_thumbnail1'];
            
            if( !isset($var_games[$i]['game_thumbnail1']) )
            {   
                $var_games[$i]['var_game_thumbnail'] = img_url('default_cover.jpg');
            }
            
            $var_games[$i]['if_support_cd'] = array();
            $var_games[$i]['if_support_dvd'] = array();
            
            if( $var_games[$i]['game_cd'] == 1 )
            {
                $var_games[$i]['if_support_cd'][] = array( 'support_type' => img_url('cd_support.png') );
            }
            else if( $var_games[$i]['game_dvd'] == 1 )
            {
                $var_games[$i]['if_support_dvd'][] = array( 'support_type' => img_url('dvd_support.png') );
            }
        }
        
        /* Setting up pagination */    
        $config = array(
            'base_url' => base_url() . 'games/pc_list/page/',
            'total_rows' => $totalRows,
            'per_page' => self::COUNT_PER_PAGE,
            'first_link' => '<<',
            'last_link' => '>>',
            'next_link' => '&gt;',
            'prev_link' => '&lt;',
            'cur_tag_open' => '&nbsp;<strong class="inactive_anchor">',
            'cur_tag_close' => '</strong>',
            'cur_page' => $start,
        );
        $this->pagination->initialize($config);

        $var_pagination = $this->pagination->create_links();
        /* End of setting up pagination */
        
        /* Header zone */
        $headerData = assets_paths();
        $headerData = array_merge($headerData, array(
            'var_game_categories' => $var_game_categories,
        ));

        /* Slider zone */
        $sliderData = assets_paths();
        $sliderData = array_merge($sliderData, array(
        ));

        /* Left zone */
        $leftData = assets_paths();
        $leftData = array_merge($leftData, array(
            'var_movie_categories' => $var_movie_categories,
            'var_game_categories' => $var_game_categories,
        ));

        /* Center zone */
        $centerData = assets_paths();
        $centerData = array_merge($centerData, array(
            'var_games' => $var_games,
            'var_pagination' => $var_pagination,
        ));
        
        /* Left zone */
        $rightData = assets_paths();
        $rightData = array_merge($rightData, array(
        ));
        
        /* Footer zone */
        $footerData = assets_paths();
        $footerData = array_merge($footerData, array(
        ));
        
        $var_header_zone = $this->parser->parse($folderPath . 'zones/header_zone.tpl', $headerData, TRUE);
        $var_slider_zone = $this->parser->parse('layout/slider_zone.tpl', $sliderData, TRUE);
        $var_left_zone = $this->parser->parse('layout/left_zone.tpl', $leftData, TRUE);
        $var_center_zone = $this->parser->parse($folderPath . 'zones/center_zone.tpl', $centerData, TRUE);
        $var_right_zone = $this->parser->parse('layout/right_zone.tpl', $rightData, TRUE);
        $var_footer_zone = $this->parser->parse('layout/footer_zone.tpl', $footerData, TRUE);
        
        $layoutData = assets_paths();
        $layoutData = array_merge($layoutData, array(
            'var_page_title' => $pageTitle,
            'css_resources' => $cssResources,
            'js_resources' => $jsResources,
            'js_literals' => $jsLiterals,

            'var_header_zone' => $var_header_zone,
            'var_slider_zone' => $var_slider_zone,
            'var_left_zone' => $var_left_zone,
            'var_center_zone' => $var_center_zone,
            'var_right_zone' => $var_right_zone,
            'var_footer_zone' => $var_footer_zone,
        ));
        
        $this->parser->parse('layout/layout.tpl', $layoutData, FALSE);
	}
}

/* End of file pc_list.php */
/* Location: ./application/controllers/pc_list.php */