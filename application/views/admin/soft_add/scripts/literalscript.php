<script type="text/javascript">
    function validateForm()
    {
        var isValid = true;
        
        var fields = ['soft_title', 'soft_ref', 'soft_link'];
        for( i = 0; i < fields.length; i++ )
        {
            var field = fields[i];
            var oField = $('#' + field);
            if( oField.val().trim() == '' )
            {
                oField.addClass('invalid-field');
                isValid = false;
            }
            else
            {
                oField.removeClass('invalid-field');
            }
        }
        
        if( ! isValid )
        {
            $('#ajax-message').addClass('error-message');
            $('#ajax-message').html('Veuillez remplir les champs obligatoires.');
        }
        else
        {
            $('#ajax-message').removeClass('error-message');
            $('#ajax-message').empty();
        }
        
        return isValid;
    }

    $(function() {
        $('#form-soft').submit(function(e) {
            var res = validateForm();
         
            $('#ajax-message').removeClass('confirmation-message');   
            if( ! res ) {
                e.preventDefault();
            }
        });
        
        $('#form-soft').ajaxForm({
            dataType: 'json',
            beforeSubmit: function() {
                $('#ajax-loading').show();
            },
            success: function(data) {
                $('#ajax-loading').hide();
                $('#ajax-message').show();
                
                if( data.error == 0 )
                {
                    $('#ajax-message').html(data.message + ' cliquer <a href="{base_url}admin/soft_edit/by_ref/' + data.ref + '" target="_blank">ici</a> pour voir');
                    $('#ajax-message').wrapInner('<strong></strong>').addClass('confirmation-message');
                    
                    //Clear the fields
                    $('#soft_title').val('');
                    $('#soft_ref').val('');
                    $('#soft_description').val('');
                    $('#soft_file_image').val('');
                    $('#soft_link').val('');
                    $('#soft_min_config').val('');
                    $('#soft_dvd').attr('checked', false);
                    $('#soft_cd').attr('checked', false);
                }
                else
                {
                    $('#ajax-message').html(data.message);
                    $('#ajax-message').wrapInner('<strong></strong>').addClass('error-message');
                }
            }
        });
    });
</script>