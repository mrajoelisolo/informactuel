<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie_Type_Model extends CI_Model
{
    const TABLE_NAME = 'movie_type';
    
    public function get_array_movie_types()
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->get()
                        ->result_array();
        return $res;
    }
}