<script type="text/javascript">
    $('#form_search').submit(function(e) {
        e.preventDefault();
        
        var currentPlatform = $('#txt_current_platform').val();
        
        var f_game_ref = $('#txt_game_ref').val();
        var f_game_title = $('#txt_game_title').val();
        
        var criteria = '';
        if( f_game_ref != '' )
            criteria += 'ref/' +  f_game_ref;
            
        if( f_game_title != '' ) {
            if(criteria != '')
                criteria += '/';
            
            criteria += 'title/' +  f_game_title;
        }
        
        document.location = '{base_url}/admin/game_list/fetch/' + currentPlatform + '/' + criteria;
    });
    
    function delete_game(game_ref)
    {
        var idDialog = '#dialog_confirm_remove_game'; 
        $( idDialog ).dialog({
            resizable: false,
            height:140,
            modal: true,
 			buttons: {
                "Confirmer": function() {
                    $.ajax({
                        url: "{base_url}admin/game_edit/delete",
                        type: "post",
                        dataType: "json",
                        data: { ref: game_ref },
                        success: function(res) {
                            $( idDialog ).dialog( "close" );
    
                            if( res.error == 0 )
                            {
                                var row = $('#' + game_ref);
                                $(row).remove();
                            }
                        }
                    });
                },
                "Annuler": function() {
                    $( idDialog ).dialog( "close" );
                }
            }
        });
    }
    
    function edit_game(game_ref)
    {
        document.location = '{base_url}admin/game_edit/edit/' + game_ref;
    }
    
    $(function() {

    });
</script>