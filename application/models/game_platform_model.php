<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_Platform_Model extends CI_Model
{
    const TABLE_NAME = 'game_platform';
    
    public function get_array_platforms()
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->get()
                        ->result_array();
        return $res;
    }
}