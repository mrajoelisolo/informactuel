<script type="text/javascript">
    function validateForm()
    {
        var isValid = true;
        
        var fields = ['movie_title', 'movie_ref'];
        for( i = 0; i < fields.length; i++ )
        {
            var field = fields[i];
            var oField = $('#' + field);
            if( oField.val().trim() == '' )
            {
                oField.addClass('invalid-field');
                isValid = false;
            }
            else
            {
                oField.removeClass('invalid-field');
            }
        }
        
        //Check the numeric fields
        var nfields = ['movie_release_year', 'movie_duration_minute'];
        for( i = 0; i < nfields.length; i++ )
        {
            var field = nfields[i];
            var oField = $('#' + field);
            var nFieldVal = oField.val();
            
            if( ! isNumber(nFieldVal) )
            {
                oField.addClass('invalid-field');
                isValid = false;
            }
            else
            {
                oField.removeClass('invalid-field');
            }
        }
        
        if( ! isValid )
        {
            $('#ajax-message').addClass('error-message');
            $('#ajax-message').html('Veuillez verifier les champs invalides.');
        }
        else
        {
            $('#ajax-message').removeClass('error-message');
            $('#ajax-message').empty();
        }
        
        return isValid;
    }

    $(function() {
        $('#form-movie').submit(function(e) {
            var res = validateForm();
         
            $('#ajax-message').removeClass('confirmation-message');   
            if( ! res ) {
                e.preventDefault();
            }
        });
        
        $('#form-movie').ajaxForm({
            dataType: 'json',
            beforeSubmit: function() {
                $('#ajax-loading').show();
            },
            success: function(data) {
                $('#ajax-loading').hide();
                $('#ajax-message').show();
                
                $('#ajax-message').html(data.message);
                if( data.error == 0 )
                {
                    document.location = '{base_url}admin/serie_list/fetch/' + data.movie_type;
                }
                else
                {
                    $('#ajax-message').wrapInner('<strong></strong>').addClass('error-message');
                }
                
            }
        });
    });
</script>