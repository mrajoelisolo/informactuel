<div class="login_form">
    <form id="form_login" method="post" action="{base_url}admin/login/authentify">
        <table class="tbl-data">
            <thead>
                <tr>
                    <th colspan="2">Inform'Actuel admin zone</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Login:</td>
                    <td><input type="text" name="txt_login" id="txt_login"></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><input type="password" name="txt_pwd" id="txt_pwd"></td>
                </tr>
                <tr>
                    <td><input class="button green" type="submit" value="Login"></td>
                    <td>
                        <img id="ajax_loading" src="{path_img}ajax-loader.gif" style="display:none;">
                        <span id="ajax_message"></span>
                    </td>
                </tr>            
            </tbody>
        </table>
    </form>
</div>