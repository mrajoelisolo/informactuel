{var_soft}
            <div class="detailsbox">
            	<div class="detailsboxleft">
                	<div class="headtitlebox"><!-- début titre -->
                    	<p class="headtitle"><a class="winsoftware"href="#">{soft_title}</a></p>
                    </div><!-- fin titre -->
                    <p class="cover"><img src="{var_soft_thumbnail}" /></p>
                	<p class="infos">{soft_description}</p>
<div class="spacer"></div>
					<p class="minconfig">Configuration minimale requise</p>
                    <p class="acteurs">{soft_config_min}</p>
                </div>
                
                <div class="detailboxright">
                        <div class="infobox">
                            <p>Genre :</p>
                            <p class="valuegenre">{category_name}</p>
                            
            <p>Référence :</p>
                            <p class="valueref">{soft_ref}</p>
                      </div>
                      {if_support_dvd}
                      <div class="support">
                      	<img class="supcd" src="{support_type}" alt="Type support" title="Type support" />
                      </div>
                      {/if_support_dvd}
                      {if_support_cd}
                      <div class="support">
                      	<img class="supcd" src="{support_type}" alt="Type support" title="Type support" />
                      </div>
                      {/if_support_cd}
                </div>

                <div class="spacer"></div>
            </div>
            <div>
            	<a href="{soft_link}" class="linkdetails">plus de détails</a>
                <div class="spacer"></div>
            </div>
{/var_soft}