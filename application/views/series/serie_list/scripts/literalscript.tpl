<script type="text/javascript">
    $(function() {
        $('#form-search').submit(function(e) {
            e.preventDefault();
            
            var keywords = $('#keywords').val();
            var category = $('#category').val();
            
            var criteria = '';
            
            if( keywords != '' ) {
                criteria += 'kwd/' + encodeURIComponent(keywords);
            }
            
            if( category != '' )
            {
                if( criteria != '' )
                    criteria += '/';
                
                criteria += 'cat/' + encodeURIComponent(category);
            }
            
            document.location.href = '{base_url}series/serie_search/show/' + criteria;
        });
    });
</script>