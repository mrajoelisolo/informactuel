<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_Add extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('game_model');
        $this->load->model('game_category_model');
        $this->load->model('game_platform_model');
    }
    
    public function index()
    {
        if($this->session->userdata('user_login'))
        {
            $pageTitle = 'Inform\'Actuel Admin - Ajout de nouveau jeux';
            $folderPath = 'admin/game_add/';
    
            $cssResources = array(
                array('var_resource' => path_css() . 'style.css'),
                array('var_resource' => path_css() . 'modal.css'),
                array('var_resource' => path_css() . 'tbldata.css'),
                array('var_resource' => path_css() . 'formvalidation.css'),
                array('var_resource' => path_css() . 'simplebutton.css'),
                
                array('var_resource' => path_css() . 'ui/jquery.ui.all.css'),
            );
            
            $jsResources = array(
                array('var_resource' => path_js() . 'jquery.min.js'),
                array('var_resource' => path_js() . 'jquery.easing-sooper.js'),
                array('var_resource' => path_js() . 'jquery.sooperfish.js'),
                array('var_resource' => path_js() . 'main.js'),
                array('var_resource' => path_js() . 'jquery.form.js'),
                
                array('var_resource' => path_js() . 'ui/jquery-ui.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.core.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.widget.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.button.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.mouse.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.draggable.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.position.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.dialog.js'),
                array('var_resource' => path_js() . 'ui/jquery.ui.datepicker.js'),
            );
            
            $jsLiterals = array(
                array('var_literal_js' => $this->parser->parse($folderPath . 'scripts/literalscript.php', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse('admin/portal/scripts/logout.php', assets_paths(), TRUE)),
            );
            
            /* Setting the header content */
            
            $headerData = assets_paths();
            $headerData = array_merge($headerData, array(
                'modal_confirm_logout' => $this->parser->parse('admin/portal/modal/confirm_logout', assets_paths(), TRUE),
            ));
            
            /* Setting the center content */
            $var_game_categories = $this->game_category_model->fetch_array_categories();
            $var_game_platforms = $this->game_platform_model->get_array_platforms();
                    
            $centerData = assets_paths();
            $centerData = array_merge($centerData, array(
                'var_game_categories' => $var_game_categories,
                'var_game_platforms' => $var_game_platforms,
            ));
            
            /* Setting the footer content */
            $footerData = assets_paths();
            $footerData = array_merge($footerData, array(
            ));
            
            $var_header_zone = $this->parser->parse($folderPath . 'header_zone', $headerData, TRUE);
            $var_center_zone = $this->parser->parse($folderPath . 'center_zone', $centerData, TRUE);
            $var_footer_zone = $this->parser->parse('footer_zone', $footerData, TRUE);
            
            $layoutData = assets_paths();
            $layoutData = array_merge($layoutData, array(
                'var_page_title' => $pageTitle,
                'css_resources' => $cssResources,
                'js_resources' => $jsResources,
                'js_literals' => $jsLiterals,
                'var_header_zone' => $var_header_zone,
                'var_center_zone' => $var_center_zone,
                'var_footer_zone' => $var_footer_zone,
            ));
            
            $this->parser->parse('layout', $layoutData, FALSE);
        }
        else
        {
            redirect('/admin/portal');
        }
    }
    
    /*
    public function save()
    {
        $response = array(
            'error'   => 0,
            'message' => 'Geronimo',
        );
        
        echo json_encode($response);
    }
    */
    
    public function save()
    {
        $game_title       = $this->input->post('game_title');
        $game_ref         = $this->input->post('game_ref');
        $category_fk    = $this->input->post('game_category');
        $game_description = $this->input->post('game_description');
        $game_platform    = $this->input->post('game_platform');
        $game_link        = $this->input->post('game_link');
        $game_config_min  = $this->input->post('game_min_config');
        $game_dvd         = ($this->input->post('game_dvd')) ? 1 : 0;
        $game_cd          = ($this->input->post('game_cd')) ? 1 : 0;
        
        $ref_exists = $this->game_model->check_if_ref_exists($game_ref);
        
        if( $ref_exists )
        {
            $response = array(
                'error'   => 1,
                'message' => 'La r&eacute;f&eacute;rence &eacute;xiste d&eacute;ja',
            );
            
            echo json_encode($response);
            return;
        }
        
        $filename = NULL;
        $response = array(
            'error'   => 0,
            'message' => 'Enregistrement effectu&eacute;e',
            'ref' => $game_ref,
        );
        
        if( isset($_FILES['game_file_image']) )
        {
            $config = array(
                'upload_path'   => './assets/images/games/thumbnails/',
                'allowed_types' => 'png|jpg|gif',
                'file_name'     => 'game_' . strtolower($game_platform) . '_' . sha1($_FILES['game_file_image']['name']), 
            );
            
            $this->upload->initialize($config);
            
            if( ! $this->upload->do_upload('game_file_image') )
            {
                $error = strip_tags($this->upload->display_errors());
                
                $response['error'] = 1;
                $response['message'] = $error;
            }
            else
            {
                $fileInfo = array(
                    'upload_data' => $this->upload->data(),
                );
                
                $filename = $fileInfo['upload_data']['file_name'];
                $img_url = base_url() . 'assets/images/games/thumbnails/' . $filename;

                $response['error'] = 0;
                $response['image'] = $img_url;
                
                //Resize the image
                $imgConfig = array(
                    'image_library'  => 'gd2',
                    'source_image'   => './assets/images/games/thumbnails/' . $filename,
                    'maintain_ratio' => TRUE,
                    'width'          => 256,
                    'height'         => 144,
                );
                
                $this->image_lib->initialize($imgConfig);
                $this->image_lib->resize();
            }
        }
        
        //Insert the game info to the database
        if( $response['error'] == 0 )
        {
            $this->game_model->save_game($game_title, $game_ref, $game_description, $game_platform, $filename,
                $game_link, $game_config_min, $game_dvd, $game_cd, $category_fk
            );
        }
        
        echo json_encode($response);
    }
}