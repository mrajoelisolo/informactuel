<div class="recentmovies"></div>
    <div class="slider">
        <div class="slideshow">
            <!-- Begin DWUser_EasyRotator -->
            <script type="text/javascript" src="http://c520866.r66.cf2.rackcdn.com/1/js/easy_rotator.min.js"></script>
            <div class="dwuserEasyRotator" style="width: 650px; height: 150px; position:relative; text-align: left; background-color:#000;" data-erconfig="{autoplayEnabled:true, lpp:'102-105-108-101-58-47-47-47-67-58-47-85-115-101-114-115-47-67-108-105-101-110-116-47-68-111-99-117-109-101-110-116-115-47-69-97-115-121-82-111-116-97-116-111-114-80-114-101-118-105-101-119-47-112-114-101-118-105-101-119-95-115-119-102-115-47', wv:1, autoplayDelay:5500, audio_controllerPadding:10}" data-ername="informslideshow" data-ertid="{ylkn3bd7ak8600651190631}">
                <div data-ertype="content" style="display: none;">           
            <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
            <!--//////////////////////////////////////////////////////////////////////// TITRES FILMS SUR SLIDER ////////////////////////////////////////////////////////////////////////-->
            <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        
                <ul data-erlabel="Main Category">
                    <li> <a class="mainLink" href="#" target="_blank"><img class="main" src="{path_img}slide4.jpg" alt="Titre du film sur Slide 1" /></a> <img class="thumb" src="{path_img}slide4.jpg" /> <span class="title">Titre du film sur Slide 1</span> <span class="desc">Petit bla bla sur le film du Slide 1</span> </li>
                    <li> <a class="mainLink" href="#" target="_blank"><img class="main" src="{path_img}slide3.jpg" alt="Titre du film sur Slide 2" /></a> <img class="thumb" src="{path_img}slide3.jpg" /> <span class="title">Twilight 5 : R&eacute;velation 2&egrave;me partie</span> <span class="desc">Apr&egrave;s la naissance de sa fille Renesm&eacute;e, Bella s'adapte peu &agrave; peu &agrave; sa nouvelle vie de vampire</span> </li>
                    <li> <a class="mainLink" href="#" target="_blank"><img class="main" src="{path_img}slide2.jpg" alt="Titre du film sur Slide 3" /></a> <img class="thumb" src="{path_img}slide2.jpg" /> <span class="title">Hansel et Gretel</span> <span class="desc"> Hansel et Gretel, devenus adultes, se lancent &agrave; la chasse aux sorci&egrave;res.</span> </li>
                    <li> <a class="mainLink" href="#" target="_blank"><img class="main" src="{path_img}slide1.jpg" alt="Titre du film sur Slide 4" /></a> <img class="thumb" src="{path_img}slide1.jpg" /> <span class="title">Les Mondes de Ralph</span> <span class="desc">Ralph le gentil m&eacute;chant d&eacute;cide de se mettre du bon c&ocirc;t&eacute;.</span> </li>
                </ul>
        
            <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
            <!--//////////////////////////////////////////////////////////////////////// FIN TITRES FILMS SUR SLIDER ////////////////////////////////////////////////////////////////////-->
            <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                          
                </div>
                <div data-ertype="layout" data-ertemplatename="NONE" style=""><ins></ins>
                    <div class="erimgMain" style="position: absolute; left: 0; right: 0; top:0; bottom:25px;" data-erconfig="{___numTiles:3, scaleMode:'fillArea', imgType:'main', __loopNextButton:false, arrowButtonMode:'rollover'}">
                        <div class="erimgMain_slides" style="position: absolute; left:0; top:0; bottom:0; right:0;">
                            <div class="erimgMain_slide">
                                <div class="erimgMain_img" style="position: absolute; left: 0; right: 0; top: 0; bottom: 0;"></div>
                                <div class="erhideWhenNoText" style="background: #F00; background: rgba(0,0,0,0.65); position: absolute; left: 0; right: 0; top: 0; padding: 10px; color: #FFF; font-family: 'Lucida Grande','Lucida Sans','Helvetica Neue',Helvetica,Arial; font-size: 10px; text-transform: uppercase;"> <span class="erimgMain_title" style="padding: 0; margin: 0; font-weight: bold;"></span> <span class="erimgMain_desc" style="padding: 0 0 0 10px; margin: 0;"></span> </div>
                            </div>
                        </div>
                        <div class="erimgMain_arrowLeft" style="position:absolute; left: 10px; top: 50%; margin-top: -15px;" data-erconfig="{image:'circleSmall', image2:'circleSmall'}"></div>
                        <div class="erimgMain_arrowRight" style="position:absolute; right: 10px; top: 50%; margin-top: -15px;"></div>
                    </div>
                    <div class="erdots" style="overflow: hidden; margin: 0; wasborder: 1px solid #00F; font-size: 10px; font-family: 'Lucida Grande', 'Lucida Sans', Arial, _sans; color: #FFF; position: absolute; left:0; right:0; bottom:1px; background-color:#000;" data-erconfig="{showText:true}" align="center">
                    <div class="erdots_wrap" style="wasbackground-color: #CFC; float: none;" align="left">
                        <!-- modify the float on this element to make left/right/none=center aligned. -->
                        <span class="erdots_btn_selected" style="padding-left: 0; width: 21px; height: 20px; display: inline-block; text-align: center; vertical-align: middle; line-height: 20px; margin: 0 2px 0 0; cursor: default; background: url({path_img}20_18_blue.png) top left no-repeat;"> &nbsp; </span> <span class="erdots_btn_normal" style="padding-left: 0; width: 21px; height: 20px; display: inline-block; text-align: center; vertical-align: middle; line-height: 20px; margin: 0 2px 0 0; cursor: pointer; background: url({path_img}20_18_black.png) top left no-repeat;"> &nbsp; </span> <span class="erdots_btn_hover" style="padding-left: 0; width: 21px; height: 20px; display: inline-block; text-align: center; vertical-align: middle; line-height: 20px; margin: 0 2px 0 0; cursor: pointer; background: url({path_img}20_18_blue.png) top left no-repeat;"> &nbsp; </span> </div>
                    </div>
                    <div class="erabout erFixCSS3" style="color: #FFF; text-align: left; background: #000; background:rgba(0,0,0,0.93); border: 2px solid #FFF; padding: 20px; font: normal 11px/14px Verdana,_sans; width: 300px; border-radius: 10px; display:none;"> This <a style="color:#FFF;" href="http://www.dwuser.com/easyrotator/" target="_blank">jQuery slider</a> was created with the free <a style="color:#FFF;" href="http://www.dwuser.com/easyrotator/" target="_blank">EasyRotator</a> software from DWUser.com. <br />
                        <br />
                        Use WordPress? The free <a style="color:#FFF;" href="http://www.dwuser.com/easyrotator/wordpress/" target="_blank">EasyRotator for WordPress</a> plugin lets you create beautiful <a style="color:#FFF;" href="http://www.dwuser.com/easyrotator/wordpress/" target="_blank">WordPress sliders</a> in seconds. <br />
                        <br />
                        <a style="color:#FFF;" href="#" class="erabout_ok">OK</a> </div>
                        <noscript>
                            Rotator powered by <a href="http://www.dwuser.com/easyrotator/">EasyRotator</a>, a free and easy jQuery slider builder from DWUser.com.  Please enable JavaScript to view.
                        </noscript>
                        <a></a>
                        <!--
                        <script type="text/javascript">/*Avoid IE gzip bug*/(function(b,c,d){try{if(!b[d]){b[d]="temp";var a=c.createElement("script");a.type="text/javascript";a.src="http://easyrotator.s3.amazonaws.com/1/js/nozip/easy_rotator.min.js";c.getElementsByTagName("head")[0].appendChild(a)}}catch(e){alert("EasyRotator fail; contact support.")}})(window,document,"er_$144");</script>
                        -->
                        <script type="text/javascript">/*Avoid IE gzip bug*/(function(b,c,d){try{if(!b[d]){b[d]="temp";var a=c.createElement("script");a.type="text/javascript";a.src="{path_js}easy-rotator.js";c.getElementsByTagName("head")[0].appendChild(a)}}catch(e){alert("EasyRotator fail; contact support.")}})(window,document,"er_$144");</script>
                </div>
            </div>
            <!-- End DWUser_EasyRotator -->
            <!--<ul>
        		<li><img src="{path_img}slide1.jpg" alt="" width="650" height="150" /></li>
        		<li><img src="{path_img}slide2.jpg" alt="" width="650" height="150" /></li>
        		<li><img src="{path_img}slide3.jpg" alt="" width="650" height="150" /></li>
        		<li><img src="{path_img}slide4.jpg" alt="" width="650" height="150" /></li>
        	</ul>-->
        </div>
    </div>
<div class="spacer"></div>