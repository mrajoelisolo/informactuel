<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie_Model extends CI_Model
{
    const TABLE_NAME = 'movie';
    const TABLE_CATEGORY = 'movie_category';
    
    public function fetch_array_latest_movies($movie_type, $start = 0, $count = 5)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                        ->where('g.category_fk = c.category_id')
                        ->where('movie_type', $movie_type)
                        ->order_by('movie_ref', 'desc')
                        ->limit($count, $start)
                        ->get()
                        ->result_array();
        
        return $res;
    }
    
    public function count_all_movies_by_type($movie_type)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('movie_type', $movie_type)
                        ->count_all_results();
        return $res;
    }
    
    public function fetch_catalog($movie_type, $movie_title, $category_name)
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.movie_type', $movie_type);
        
        if( trim($movie_title) != '' )
            $this->db->like('g.movie_title', $movie_title, 'both');
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
        
        $res = $this->db->order_by('g.movie_title', 'asc')
                        ->get()
                        ->result_array();
                        
        return $res;
    }
    
    public function save_movie($movie_title, $movie_ref, $movie_description, $movie_type, $movie_preview, $movie_thumbnail1, $movie_thumbnail1,
        $movie_dvd, $movie_cd, $movie_actors_tag, $category_fk, $movie_director, $movie_release_year, $movie_duration_minute, $movie_url)
    {
        $this->db->set('movie_title', $movie_title);
        $this->db->set('movie_ref', $movie_ref);
        $this->db->set('movie_description', $movie_description);
        $this->db->set('movie_type', $movie_type);
        $this->db->set('movie_preview', $movie_preview);
        $this->db->set('movie_thumbnail1', $movie_thumbnail1);
        $this->db->set('movie_dvd', $movie_dvd);
        $this->db->set('movie_cd', $movie_cd);
        $this->db->set('movie_actors_tag', $movie_actors_tag);
        $this->db->set('movie_director', $movie_director);
        $this->db->set('movie_release_year', $movie_release_year);
        $this->db->set('movie_duration_minute', $movie_duration_minute);
        $this->db->set('movie_url', $movie_url);
        $this->db->set('category_fk', $category_fk);
        $this->db->insert(self::TABLE_NAME);
    }
    
    public function check_if_ref_exists($movie_ref)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('movie_ref', $movie_ref)
                        ->count_all_results();
        
        return ($res > 0);
    }
    
    public function fetch_array_movies_by($start = 0, $count = 5, $movie_type, $movie_title = '', $movie_ref = '',$category_name = '', $order_ref_by = 'desc')
    {   
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.movie_type', $movie_type);
                 
        if( trim($movie_title) != '' )
            $this->db->like('g.movie_title', $movie_title, 'both');
        
        if( trim($movie_ref) != '' )
            $this->db->where('g.movie_ref', $movie_ref);
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
                        
        $res = $this->db->order_by('g.movie_ref', $order_ref_by)
                        ->limit($count, $start)
                        ->get()
                        ->result_array();

        return $res;
    }
    
    public function count_movies_by($movie_type, $movie_title = '', $movie_ref = '', $category_name = '')
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                 ->where('g.category_fk = c.category_id')
                 ->where('g.movie_type', $movie_type);
        
        if( trim($movie_title) != '' )
            $this->db->like('g.movie_title', $movie_title, 'both');
            
        if( trim($movie_ref) != '' )
            $this->db->where('g.movie_ref', $movie_ref);
        
        if( trim($category_name) != '' )
            $this->db->where('c.category_name', $category_name);
            
        $res = $this->db->count_all_results();
        
        return $res;
    }
    
    public function fetch_array_one_movie_by_ref($movie_ref) {
        $res = $this->db->select('*')
                    ->from(self::TABLE_NAME . ' g, ' . self::TABLE_CATEGORY . ' c')
                    ->where('g.category_fk = c.category_id')
                    ->where('g.movie_ref', $movie_ref)
                    ->get()
                    ->row_array();
                 
        return $res;
    }
    
    public function fetch_array_one_last_movie_by_type($type)
    {
        $res = $this->db->query('SELECT * FROM ' . self::TABLE_NAME . ' m
                                WHERE movie_type = ?
                                AND m.movie_ref = (SELECT MAX(movie_ref) FROM ' . self::TABLE_NAME . '
                                WHERE movie_type = ?);', array(
                                    $type, $type
                                ))
                        ->row_array();
                 
        return $res;
    }
    
    public function update_movie_by_ref($movie_title, $movie_ref, $movie_old_ref, $movie_description, $movie_type, $movie_preview, $movie_thumbnail1,
        $movie_dvd, $movie_cd, $movie_actors_tag, $category_fk, $movie_director, $movie_release_year, $movie_duration_minute, $movie_url)
    {
        $data = array(
            'movie_ref' => $movie_ref,
            'movie_title' => $movie_title,
            'movie_ref' => $movie_ref,
            'movie_description' => $movie_description,
            'movie_type' => $movie_type,
            'movie_preview' => $movie_preview,
            'movie_dvd' => $movie_dvd,
            'movie_cd' => $movie_cd,
            'movie_actors_tag' => $movie_actors_tag,
            'movie_director' => $movie_director,
            'movie_release_year' => $movie_release_year,
            'movie_duration_minute' => $movie_duration_minute,
            'movie_url' => $movie_url,
            'category_fk' => $category_fk,
        );
        
        if( ! empty($movie_thumbnail1) )
        {
            $data['movie_thumbnail1'] = $movie_thumbnail1;
        }
        
        $this->db->where('movie_ref', $movie_old_ref);
        $this->db->update(self::TABLE_NAME, $data);
    }
    
    public function delete_movie_by_ref($movie_ref)
    {
        $this->db->where('movie_ref', $movie_ref);
        $this->db->delete(self::TABLE_NAME);
    }
}