<div id="sidebar_container">
    <div class="sidebar">
        <h3>Actions</h3>
        <div class="sidebar_item">
            <p>
                <input class="button purple" type="button" value="Fermer la session" onclick="logout();">
            </p>
        </div>
    </div>
</div>
<div class="content">
    <h1>Liste des séries</h1>
    
    <div class="content_item">
        <form id="form_search" method="post" action="#">
            <input type="hidden" id="txt_current_type" value="{var_current_type}">
            <p>
                <label for="movie_ref"><strong>R&eacute;f&eacute;rence de la série: </strong></label>
                <input type="text" name="txt_movie_ref" id="txt_movie_ref">
                <label for="movie_ref"><strong>Titre de la série: </strong></label>
                <input type="text" name="txt_movie_title" id="txt_movie_title">
            </p>
            <p>
                <input class="button darkblue" type="submit" value="Rechercher">
            </p>
        </form>
    </div>
    
    <div class="wrap-pagination">
        {var_pagination}
    </div>
    
    <table class="tbl-data">
        <thead>
            <tr>
                <th>R&eacute;f&eacute;rence</th>
                <th>Titre</th>
                <th>Cat&eacute;gorie</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            {var_movies}
            <tr id="{movie_ref}">
                <td>{movie_ref}</td>
                <td>{movie_title}</td>
                <td>{category_name}</td>
                <td>
                    <input class="button green" type="button" value="Modifier" onclick="edit_movie('{movie_ref}');">
                    <input class="button orange" type="button" value="Supprimer" onclick="delete_movie('{movie_ref}');">
                </td>
            </tr>
            {/var_movies}
        </tbody>
    </table>
    
    <div class="wrap-pagination">
        {var_pagination}
    </div>
</div>