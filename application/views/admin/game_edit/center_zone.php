<div id="sidebar_container">
    <div class="sidebar">
        <h3>Actions</h3>
        <div class="sidebar_item">
            <p>
                <input class="button purple" type="button" value="Fermer la session" onclick="logout();">
            </p>
        </div>
    </div>
</div>
<div class="content">
    <h1>Edition de jeu</h1>
    <form id="form-game" action="{base_url}admin/game_edit/update" enctype="multipart/form-data" method="post">
        <input name="MAX_FILE_SIZE" type="hidden" value="1000000">
        <input type="hidden" name="game_old_ref" value="{var_game_ref}">
        <table class="tbl-data">
            <thead>
                <tr>
                    <th colspan="2">General</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="col1"><strong>Game title *</strong></td>
                    <td class="col2"><input type="text" id="game_title" name="game_title" class="input-txt" value="{var_game_title}"></td>
                </tr>
                
                <tr>
                    <td><strong>Game reference *</strong></td>
                    <td><input type="text" id="game_ref" name="game_ref" value="{var_game_ref}"></td>
                </tr>
                
                <tr>
                    <td><strong>Game category</strong></td>
                    <td>
                        <select id="game_category" class="select-cmb" name="game_category">
                            {var_game_categories}
                            <option {selected_game_category} value="{category_id}">{category_name}</option>
                            {/var_game_categories}
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td><strong>Game description</strong></td>
                    <td><textarea id="game_description" name="game_description">{var_game_description}</textarea></td>
                </tr>
                
                <tr>
                    <td><strong>Platform</strong></td>
                    <td>
                        <select id="game_platform" class="select-cmb" name="game_platform">
                            {var_game_platforms}
                            <option {selected_game_platform} value="{platform_name}">{platform_name}</option>
                            {/var_game_platforms}
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td><strong>Game cover</strong></td>
                    <td><input type="file" id="game_file_image" name="game_file_image" accept="image/*"></td>
                </tr>
                
                <tr>
                    <td><strong>Game link *</strong></td>
                    <td><input class="input-txt" type="text" id="game_link" name="game_link" value="{var_game_link}"></td>
                </tr>
                
                <tr>
                    <td><strong>Minimal config</strong></td>
                    <td><textarea id="game_min_config" name="game_min_config">{var_config_min}</textarea></td>
                </tr>
                
                <tr>
                    <td><strong>Supports</strong></td>
                    <td>
                        <input type="checkbox" id="game_dvd" name="game_dvd" value="game_dvd" {var_dvd_checked}> DVD<br>
                        <input type="checkbox" id="game_cd" name="game_cd" value="game_cd" {var_cd_checked}> CD
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Mettre à jour" class="button green">
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="div-message">
            <img id="ajax-loading" src="{path_img}ajax-loader.gif" style="display:none;">
            <span id="ajax-message"></span>
        </div>
    </form>
</div>