<div id="logo">
    <div id="logo_text">
        <h1>Inform'Actuel</h1>
        <h2>La r&eacute;f&eacute;rence en jeux, logiciels, films et s&eacute;ries TV</h2>
    </div>
</div>
<nav>
    <ul class="sf-menu" id="nav">
        <li><a href="{base_url}admin/homepage">Accueil</a></li>
        <li><a href="{base_url}admin/game_add">Nouveau jeu</a></li>
        <li><a href="{base_url}admin/game_list/fetch/pc">Liste des jeux PC</a></li>
        <li><a href="{base_url}admin/game_list/fetch/ps2">Liste des jeux PlayStation 2</a></li>
        <li><a href="{base_url}admin/game_list/fetch/xbox360">Liste des jeux XBox 360</a></li>
    </ul>
</nav>
{modal_confirm_logout}